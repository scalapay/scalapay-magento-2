<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Model;

use Exception;
use Scalapay\Scalapay\Gateway\Settings\Scalapay\Settings as ScalapaySettings;
use Scalapay\Scalapay\Helper\Payment as PaymentHelper;
use Scalapay\Sdk\Api as ScalapayApi;
use Scalapay\Sdk\Model\Api\Client;

/**
 * Class ApiClient
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Model
 */
class ApiClient
{
    /** @var ScalapaySettings $scalapaySettings */
    private $scalapaySettings;

    /** @var PaymentHelper $paymentHelper */
    private $paymentHelper;

    /**
     * ApiClient constructor.
     *
     * @param ScalapaySettings $scalapaySettings
     * @param PaymentHelper $paymentHelper
     */
    public function __construct(
        ScalapaySettings $scalapaySettings,
        PaymentHelper $paymentHelper
    ) {
        $this->scalapaySettings = $scalapaySettings;
        $this->paymentHelper = $paymentHelper;
    }

    /**
     * Returns the client by the given scalapay payment type.
     *
     * @param string $scalapayType
     * @param int|null $storeId
     * @param string|null $apiKey
     * @param string|null $apiEndpoint
     * @return ScalapayApi
     * @throws Exception
     */
    public function execute(
        string $scalapayType,
        ?int $storeId = null,
        ?string $apiKey = null,
        ?string $apiEndpoint = null
    ): ScalapayApi {
        // check scalapay payment type
        if (!$this->paymentHelper->isScalapayPayment($scalapayType)) {
            throw new Exception('Invalid Scalapay payment type.');
        }

        // return a custom client
        if ($apiKey && $apiEndpoint) {
            return ScalapayApi::configure($apiKey, $apiEndpoint);
        }

        // get mode
        $isLiveMode = $this->scalapaySettings->getLiveMode($storeId);

        // return test client
        if (!$isLiveMode) {
            return ScalapayApi::configure(
                $this->scalapaySettings->getTestApiKey($storeId),
                Client::SANDBOX_URI
            );
        }

        // return live client
        return ScalapayApi::configure($this->scalapaySettings->getProductionApiKey($storeId));
    }
}
