<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Model\Filters;

use Magento\Quote\Api\Data\CartInterface;
use Scalapay\Scalapay\Gateway\Settings\Scalapay\Settings as ScalapaySettings;
use Scalapay\Scalapay\Model\Filters\Interfaces\FilterInterface;

/**
 * Class CartAmountRange
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Model\Filters
 */
class CartAmountRange implements FilterInterface
{
    /** @var ScalapaySettings $scalapaySettings */
    private $scalapaySettings;

    /**
     * CartAmountRange constructor.
     */
    public function __construct(
        ScalapaySettings $scalapaySettings
    ) {
        $this->scalapaySettings = $scalapaySettings;
    }

    /**
     * Returns true if the quote base grand total is included between min and max amount else false.
     *
     * @param CartInterface $quote
     * @return bool
     */
    public function execute(CartInterface $quote): bool
    {
        // get min and max amount by the given scalapay configuration type
        $minAmount = $this->scalapaySettings->getMinAmount();
        $maxAmount = $this->scalapaySettings->getMaxAmount();

        // check base grand total
        return $quote->getBaseGrandTotal() >= $minAmount && $quote->getBaseGrandTotal() <= $maxAmount;
    }
}
