<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Model\Filters;

use Exception;
use Magento\Backend\Model\Locale\Resolver as LocaleResolver;
use Magento\Quote\Api\Data\CartInterface;
use Scalapay\Scalapay\Helper\Restriction as RestrictionHelper;
use Scalapay\Scalapay\Model\Filters\Interfaces\FilterInterface;

/**
 * Class DisabledLanguages
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Model\Filters
 */
class DisabledLanguages implements FilterInterface
{
    /** @var LocaleResolver $localeResolver */
    private $localeResolver;

    /** @var RestrictionHelper $restrictionHelper */
    private $restrictionHelper;

    /**
     * DisabledLanguages constructor.
     *
     * @param LocaleResolver $localeResolver
     * @param RestrictionHelper $restrictionHelper
     */
    public function __construct(
        LocaleResolver $localeResolver,
        RestrictionHelper $restrictionHelper
    ) {
        $this->localeResolver = $localeResolver;
        $this->restrictionHelper = $restrictionHelper;
    }

    /**
     * Returns true if the given language is not disabled else false.
     *
     * @param CartInterface $quote
     * @return bool
     * @throws Exception
     */
    public function execute(CartInterface $quote): bool
    {
        $currentLanguage = $this->localeResolver->getLocale();
        return !$this->restrictionHelper->isLanguageDisabled($currentLanguage);
    }
}
