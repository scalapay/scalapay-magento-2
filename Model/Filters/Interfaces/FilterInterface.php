<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Model\Filters\Interfaces;

use Magento\Quote\Api\Data\CartInterface;

/**
 * Interface FilterInterface
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Model\Filters\Interfaces
 */
interface FilterInterface
{
    /**
     * Executes the filter.
     *
     * @param CartInterface $quote
     * @return bool
     */
    public function execute(CartInterface $quote): bool;
}
