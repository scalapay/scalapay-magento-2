<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Model\Filters;

use Exception;
use Magento\Quote\Api\Data\CartInterface;
use Scalapay\Scalapay\Helper\Restriction as RestrictionHelper;
use Scalapay\Scalapay\Model\Filters\Interfaces\FilterInterface;

/**
 * Class DisabledCountries
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Model\Filters
 */
class DisabledCountries implements FilterInterface
{
    /** @var RestrictionHelper $restrictionHelper */
    private $restrictionHelper;

    /**
     * DisabledCountries constructor.
     *
     * @param RestrictionHelper $restrictionHelper
     */
    public function __construct(
        RestrictionHelper $restrictionHelper
    ) {
        $this->restrictionHelper = $restrictionHelper;
    }

    /**
     * Returns true if the given country is not disabled else false.
     *
     * @param CartInterface $quote
     * @return bool
     * @throws Exception
     */
    public function execute(CartInterface $quote): bool
    {
        // get quote billing country code
        $quoteBillingCountry = $quote->getBillingAddress()->getCountryId();
        if (!$quoteBillingCountry) {
            $quoteBillingCountry = '';
        }

        // return if the country is not disabled
        return !$this->restrictionHelper->isCountryDisabled($quoteBillingCountry);
    }
}
