<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Model\Filters;

use Exception;
use Magento\Quote\Api\Data\CartInterface;
use Scalapay\Scalapay\Helper\Restriction as RestrictionHelper;
use Scalapay\Scalapay\Model\Filters\Interfaces\FilterInterface;

/**
 * Class DisabledProductTypes
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Model\Filters
 */
class DisabledProductTypes implements FilterInterface
{
    /** @var RestrictionHelper $restrictionHelper */
    private $restrictionHelper;

    /**
     * DisabledProductTypes constructor.
     *
     * @param RestrictionHelper $restrictionHelper
     */
    public function __construct(
        RestrictionHelper $restrictionHelper
    ) {
        $this->restrictionHelper = $restrictionHelper;
    }

    /**
     * Returns true if the quote has disabled product types else false.
     *
     * @param CartInterface $quote
     * @return bool
     * @throws Exception
     */
    public function execute(CartInterface $quote): bool
    {
        $productIds = array_map(static function ($item) {
            return (int) $item->getProductId();
        }, $quote->getAllItems());

        return !$this->restrictionHelper->isProductListInDisabledProductTypes($productIds);
    }
}
