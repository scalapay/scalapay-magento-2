<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Model\Filters;

use Exception;
use Magento\Quote\Api\Data\CartInterface;
use Scalapay\Scalapay\Helper\Restriction as RestrictionHelper;
use Scalapay\Scalapay\Model\Filters\Interfaces\FilterInterface;

/**
 * Class DisabledCurrencies
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Model\Filters
 */
class DisabledCurrencies implements FilterInterface
{
    /** @var RestrictionHelper $restrictionHelper */
    private $restrictionHelper;

    /**
     * DisabledCurrencies constructor.
     *
     * @param RestrictionHelper $restrictionHelper
     */
    public function __construct(
        RestrictionHelper $restrictionHelper
    ) {
        $this->restrictionHelper = $restrictionHelper;
    }

    /**
     * Returns true if the currency is not disabled else false.
     *
     * @param CartInterface $quote
     * @return bool
     * @throws Exception
     */
    public function execute(CartInterface $quote): bool
    {
        $currencyCode = $quote->getStore()->getCurrentCurrencyCode();
        return !$this->restrictionHelper->isCurrencyDisabled($currencyCode);
    }
}
