<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Model;

use Exception;
use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Api\OrderRepositoryInterface;
use Scalapay\Scalapay\Logger\Logger;

/**
 * Class DelayedCapture
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Model
 */
class DelayedCapture
{
    /** @var string DELAYED_CAPTURE_STATUS_OK */
    const DELAYED_CAPTURE_STATUS_OK = 'AUTHORIZED';

    /** @var OrderRepositoryInterface $orderRepositoryInterface */
    private $orderRepositoryInterface;

    /** @var ApiClient $apiClient */
    private $apiClient;

    /** @var Logger $logger */
    private $logger;

    /**
     * Capture constructor.
     *
     * @param OrderRepositoryInterface $orderRepositoryInterface
     * @param ApiClient $apiClient
     * @param Logger $logger
     */
    public function __construct(
        OrderRepositoryInterface $orderRepositoryInterface,
        ApiClient $apiClient,
        Logger $logger
    ) {
        $this->orderRepositoryInterface = $orderRepositoryInterface;
        $this->apiClient = $apiClient;
        $this->logger = $logger;
    }

    /**
     * Extends the expiration of the payment authorization to permits the capture with delay.
     *
     * @param int $orderId
     * @return void
     * @throws Exception
     */
    public function execute(int $orderId): array
    {
        try {
            // log
            $this->logger->info(__CLASS__ . ': Start');

            // get order
            $order = $this->orderRepositoryInterface->get($orderId);
            if (!$order || !$order->getId()) {
                throw new Exception('Invalid order.');
            }

            // get payment method
            $paymentMethod = $order->getPayment()->getMethod();
            if (!$paymentMethod) {
                throw new Exception('Invalid payment method.');
            }

            // get order token
            $orderToken = $order->getScalapayOrderToken();
            if (!$orderToken) {
                throw new Exception('Invalid order token.');
            }

            // log
            $this->logger->info(__CLASS__ . ': Order Id: ' . $order->getId());

            // get api client
            $apiClient = $this->apiClient->execute($paymentMethod);

            // delay capture on Scalapay side
            $delayCapture = $apiClient->delayCapture($orderToken);
            if (!isset($delayCapture['body']['status'])
                || strtolower($delayCapture['body']['status']) !== strtolower(self::DELAYED_CAPTURE_STATUS_OK)
            ) {
                $message = $delayCapture['body']['message'] ?? 'Impossible capture with delay the order ' . $orderId;
                throw new Exception($message);
            }

            // log
            $this->logger->info(__CLASS__ . ': End');

            // return response
            return [];
        } catch (Exception $e) {
            $this->logger->critical(__CLASS__ . ': ' . $e->getMessage());
            throw new LocalizedException(__('Payment delay capture was not authorized from Scalapay.'));
        }
    }
}
