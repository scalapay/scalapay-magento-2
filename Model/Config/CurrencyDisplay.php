<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Model\Config;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class CurrencyDisplay
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Model\Config
 */
class CurrencyDisplay implements OptionSourceInterface
{
    /**
     * Returns currency display option array.
     *
     * @return string[][]
     */
    public function toOptionArray(): array
    {
        return [
            ['value' => 'symbol', 'label' => 'Symbol (€)'],
            ['value' => 'code', 'label' => 'Code (EUR)']
        ];
    }
}
