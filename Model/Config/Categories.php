<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Model\Config;

use Exception;
use Magento\Catalog\Model\CategoryFactory;
use Magento\Catalog\Model\ResourceModel\Category\Collection as CategoryCollection;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory as CategoryCollectionFactory;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Data\OptionSourceInterface;
use Magento\Framework\Exception\LocalizedException;
use Scalapay\Scalapay\Helper\System as SystemHelper;
use Scalapay\Scalapay\Logger\Logger;

/**
 * Class Categories
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Model\Config
 */
class Categories implements OptionSourceInterface
{
    /** @var string EAV_ENTITY_TYPE_TABLE */
    const EAV_ENTITY_TYPE_TABLE = 'eav_entity_type';

    /** @var string EAV_ATTRIBUTE_TABLE */
    const EAV_ATTRIBUTE_TABLE = 'eav_attribute';

    /** @var string CATALOG_CATEGORY_ENTITY_VARCHAR_TABLE */
    const CATALOG_CATEGORY_ENTITY_VARCHAR_TABLE = 'catalog_category_entity_varchar';

    /** @var CategoryFactory $categoryFactory */
    private $categoryFactory;

    /** @var CategoryCollectionFactory $categoryCollectionFactory */
    private $categoryCollectionFactory;

    /** @var ResourceConnection $resourceConnection */
    private $resourceConnection;

    /** @var SystemHelper $systemHelper */
    private $systemHelper;

    /** @var Logger $logger */
    private $logger;

    /**
     * Categories constructor.
     *
     * @param CategoryFactory $categoryFactory
     * @param CategoryCollectionFactory $categoryCollectionFactory
     * @param ResourceConnection $resourceConnection
     * @param SystemHelper $systemHelper
     * @param Logger $logger
     */
    public function __construct(
        CategoryFactory $categoryFactory,
        CategoryCollectionFactory $categoryCollectionFactory,
        ResourceConnection $resourceConnection,
        SystemHelper $systemHelper,
        Logger $logger
    ) {
        $this->categoryFactory = $categoryFactory;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->resourceConnection = $resourceConnection;
        $this->systemHelper = $systemHelper;
        $this->logger = $logger;
    }

    /**
     * Returns category collection.
     *
     * @return CategoryCollection
     * @throws LocalizedException
     */
    public function getCategoryCollection(): CategoryCollection
    {
        // create collection
        $collection = $this->categoryCollectionFactory->create();
        $collection->addAttributeToSelect('*');

        // select only active categories
        $collection->addIsActiveFilter();

        // return category collection
        return $collection;
    }

    /**
     * Returns categories option array.
     *
     * @return array
     * @throws LocalizedException
     */
    public function toOptionArray(): array
    {
        $categoriesArray = $this->toArray();
        $categories = [];

        foreach ($categoriesArray as $value => $label) {
            $categories[] = [
                'value' => $value,
                'label' => $label
            ];
        }

        return $categories;
    }

    /**
     * Parses and returns category collection to array.
     *
     * @return array
     * @throws LocalizedException
     */
    private function toArray(): array
    {
        $categories = $this->getCategoryCollection();
        $categoryList = [];
        $categoryList[''] = __('-- No Restrictions --');
        foreach ($categories as $category) {
            $categoryList[$category->getEntityId()] = $this->getParentNames($category->getPath()) . $category->getName();
        }
        return $categoryList;
    }

    /**
     * Returns the category parent names.
     *
     * @param string $path
     * @return string
     */
    private function getParentNames(string $path = ''): string
    {
        // init vars
        $parentNames = '';
        $rootCategories = [1, 2];

        // get category tree
        $categoryTree = explode('/', $path);

        // remove last category child from the category tree
        array_pop($categoryTree);

        // get category names array
        $categoryNames = $this->getCategoryNames();

        // check category tree
        if (!$categoryTree || (count($categoryTree) < count($rootCategories))) {
            return $parentNames;
        }

        // loop the category tree
        foreach ($categoryTree as $categoryId) {
            // check if category is not part of root categories
            if (!in_array($categoryId, $rootCategories)) {
                // build category parent name
                $categoryName = $categoryNames[(int) $categoryId] ?? '';
                $parentNames .= $categoryName . ' -> ';
            }
        }

        // return parent names
        return $parentNames;
    }

    /**
     * Returns category names array.
     *
     * @return array
     */
    private function getCategoryNames(): array
    {
        try {
            // init vars
            $categoryNames = [];

            // get connection
            $connection = $this->resourceConnection->getConnection();

            // get catalog category entity type id
            $eavEntityTypeTableName = $this->resourceConnection->getTableName(self::EAV_ENTITY_TYPE_TABLE);
            $query = $connection->select()
                ->from($eavEntityTypeTableName, ['entity_type_id'])
                ->where('entity_type_code = (?)', 'catalog_category');
            $entityTypeId = $connection->fetchOne($query);

            // get catalog category name attribute id
            $eavAttributeTableName = $this->resourceConnection->getTableName(self::EAV_ATTRIBUTE_TABLE);
            $query = $connection->select()
                ->from($eavAttributeTableName, ['attribute_id'])
                ->where('attribute_code = (?)', 'name')
                ->where('entity_type_id = (?)', $entityTypeId);
            $attributeId = $connection->fetchOne($query);

            // get category names
            $catalogCategoryEntityVarcharTableName = $this->resourceConnection->getTableName(
                self::CATALOG_CATEGORY_ENTITY_VARCHAR_TABLE
            );
            $query = $connection->select()
                ->from($catalogCategoryEntityVarcharTableName, [$this->getPrimaryKey(), 'value'])
                ->where('attribute_id = (?)', $attributeId);
            $categories = $connection->fetchAll($query);

            // parse category names array
            foreach ($categories as $category) {
                $categoryNames[(int) $category[$this->getPrimaryKey()]] = $category['value'];
            }

            // return category names array
            return $categoryNames;
        } catch (Exception $e) {
            // log exception
            $this->logger->critical(
                __CLASS__ .
                ': Impossible retrieve category names: ' .
                $e->getMessage()
            );

            // return empty array
            return [];
        }
    }

    /**
     * Returns the primary key.
     *
     * @return string
     */
    private function getPrimaryKey(): string
    {
        return $this->systemHelper->isCommunityEdition() ? 'entity_id' : 'row_id';
    }
}
