<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Model\Config;

use Magento\Framework\Data\OptionSourceInterface;
use Scalapay\Scalapay\Helper\Restriction as RestrictionHelper;

/**
 * Class Countries
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Model\Config
 */
class Countries implements OptionSourceInterface
{
    /**
     * Returns countries.
     *
     * @return string[]
     */
    public function toOptionArray(): array
    {
        // init countries array
        $countries = [];

        // add 'no restriction' label
        $countries[''] = [
            'value' => '',
            'label' => __('-- No Restrictions --')
        ];

        // build countries array
        foreach (RestrictionHelper::ALLOWED_COUNTRIES as $countryOption) {
            $countries[] = [
                'value' => $countryOption['value'],
                'label' => $countryOption['label']
            ];
        }

        // return countries
        return $countries;
    }
}
