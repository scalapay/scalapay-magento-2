<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Model\Config;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class CurrencyPosition
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Model\Config
 */
class CurrencyPosition implements OptionSourceInterface
{
    /**
     * Returns currency position option array.
     *
     * @return string[][]
     */
    public function toOptionArray(): array
    {
        return [
            ['value' => 'after', 'label' => 'After'],
            ['value' => 'before', 'label' => 'Before']
        ];
    }
}
