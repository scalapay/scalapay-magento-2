<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Model\Config;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class AmountSeparator
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Model\Config
 */
class AmountSeparator implements OptionSourceInterface
{
    /**
     * Returns amount separator option array.
     *
     * @return string[][]
     */
    public function toOptionArray(): array
    {
        return [
            ['value' => 'auto', 'label' => 'Auto'],
            ['value' => '.', 'label' => 'Dot'],
            ['value' => ',', 'label' => 'Comma']
        ];
    }
}
