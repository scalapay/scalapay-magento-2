<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Model\Config;

use Magento\Framework\Data\OptionSourceInterface;
use Scalapay\Scalapay\Helper\Restriction as RestrictionHelper;

/**
 * Class Languages
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Model\Config
 */
class Languages implements OptionSourceInterface
{
    /**
     * Returns languages.
     *
     * @return string[]
     */
    public function toOptionArray(): array
    {
        // init languages array
        $languages = [];

        // add 'no restriction' label
        $languages[''] = [
            'value' => '',
            'label' => __('-- No Restrictions --')
        ];

        // build languages array
        foreach (RestrictionHelper::ALLOWED_LANGUAGES as $languageOption) {
            $languages[] = [
                'value' => $languageOption['value'],
                'label' => $languageOption['label']
            ];
        }

        // return languages
        return $languages;
    }
}
