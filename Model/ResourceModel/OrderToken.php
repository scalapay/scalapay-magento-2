<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Model\ResourceModel;

use Exception;
use Magento\Framework\App\ResourceConnection;

/**
 * Class OrderToken
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Model\ResourceModel
 */
class OrderToken
{
    /** @var string ENTITY_ID_COLUMN */
    const ENTITY_ID_COLUMN = 'entity_id';

    /** @var string ORDER_TOKEN_COLUMN */
    const ORDER_TOKEN_COLUMN = 'scalapay_order_token';

    /** @var string QUOTE_TABLE */
    const QUOTE_TABLE = 'quote';

    /** @var string SALES_ORDER_TABLE */
    const SALES_ORDER_TABLE = 'sales_order';

    /** @var ResourceConnection $resourceConnection */
    private $resourceConnection;

    /**
     * OrderToken constructor.
     *
     * @param ResourceConnection $resourceConnection
     */
    public function __construct(
        ResourceConnection $resourceConnection
    ) {
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * Saves Scalapay order token on the given model entity by the given entity id.
     *
     * @param string $modelEntity
     * @param int $entityId
     * @param string $orderToken
     * @return bool
     */
    private function save(string $modelEntity, int $entityId, string $orderToken): bool
    {
        try {
            $connection = $this->resourceConnection->getConnection();
            $connection->update(
                $this->resourceConnection->getTableName($modelEntity),
                [self::ORDER_TOKEN_COLUMN => $orderToken],
                self::ENTITY_ID_COLUMN . ' = ' . $entityId
            );
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Retrieves a field on the given model by the given Scalapay order token.
     *
     * @param string $modelEntity
     * @param string $orderToken
     * @return string|false
     */
    private function get(string $modelEntity, string $orderToken)
    {
        try {
            $connection = $this->resourceConnection->getConnection();
            $select = $connection->select()
                ->from($this->resourceConnection->getTableName($modelEntity))
                ->where(self::ORDER_TOKEN_COLUMN . ' = ?', $orderToken);

            return $connection->fetchOne($select) ?? false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Saves Scalapay order token on quote table.
     *
     * @param int $quoteId
     * @param string $orderToken
     * @return bool
     */
    public function saveOnQuoteTable(int $quoteId, string $orderToken): bool
    {
        return $this->save(OrderToken::QUOTE_TABLE, $quoteId, $orderToken);
    }

    /**
     * Returns the quote id by the given order token.
     *
     * @param string $orderToken
     * @return string|false
     */
    public function getQuoteId(string $orderToken)
    {
        return $this->get(self::QUOTE_TABLE, $orderToken);
    }
}
