<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Model;

use Exception;
use Scalapay\Scalapay\Gateway\Settings\Scalapay\Settings as ScalapaySettings;
use Scalapay\Scalapay\Logger\Logger;
use Scalapay\Sdk\Service\Curl;

/**
 * Class ApiConfiguration
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Model
 */
class ApiConfiguration
{
    /** @var ScalapaySettings $scalapaySettings */
    private $scalapaySettings;

    /** @var Logger $logger */
    private $logger;

    /** @var ApiClient $apiClient */
    private $apiClient;

    /** @var Curl $curl */
    private $curl;

    /** @var array $cache */
    private $cache;

    /** @var array $configuration */
    private $config;

    /**
     * ApiConfiguration constructor.
     *
     * @param ScalapaySettings $scalapaySettings
     * @param Logger $logger
     * @param Curl $curl
     * @param ApiClient $apiClient
     */
    public function __construct(
        ScalapaySettings $scalapaySettings,
        Logger $logger,
        ApiClient $apiClient,
        Curl $curl
    ) {
        $this->scalapaySettings = $scalapaySettings;
        $this->logger = $logger;
        $this->apiClient = $apiClient;
        $this->curl = $curl;
        $this->cache = [];
        $this->config = [];
    }

    /**
     * Returns the Scalapay API configuration.
     *
     * @param array $settings
     * @return array
     * @throws Exception
     */
    public function get(array $settings = []): array
    {
        // return configuration if the array is already filled
        if (count($this->config)) {
            return $this->config;
        }

        // get settings
        $settings = count($settings) ? $settings : $this->getSettings();

        // get configurations
        foreach ($settings as $env => $config) {
            // get endpoint and secret key
            $endpoint = $config['endpoint'];
            $secretKey = $config['secretKey'];

            // check endpoint and secret key
            if (!$endpoint || !$secretKey) {
                $this->config[$env] = [];
                continue;
            }

            // calculate fingerprint by endpoint and secret key combination
            $fingerprint = base64_encode($endpoint . $secretKey);

            // skip call already done
            if (isset($this->cache[$fingerprint])) {
                $this->config[$env] = $this->cache[$fingerprint];
                continue;
            }

            // get api client
            $apiClient = $this->apiClient->execute(
                ScalapaySettings::CODE,
                null,
                $secretKey,
                $endpoint
            );

            // get or skip
            try {
                // get merchant configuration
                $response = $apiClient->getMerchantConfiguration();
            } catch (Exception $e) {
                // log data
                $this->logger->critical(__CLASS__ . ': ' . $e->getMessage());

                // skip
                $this->config[$env] = [];
                continue;
            }

            // loop configurations
            foreach ($response['body'] as $keys) {
                // skip if the type is not online
                if ($keys['type'] !== 'online') {
                    continue;
                }

                // avoid to consider not stackable products
                // don't apply this check to pay in 3 because it is enabled as default
                if ($keys['product'] !== 'pay-in-3' && !$keys['isStackable']) {
                    continue;
                }

                // get min and max amount
                $minAmount = $keys['configuration']['minimumAmount']['amount'] ?? null;
                $maxAmount = $keys['configuration']['maximumAmount']['amount'] ?? null;

                // fill configuration by product type
                switch ($keys['product']) {
                    case 'pay-in-3':
                        $this->config[$env]['payin3']['minAmount'] = $minAmount;
                        $this->config[$env]['payin3']['maxAmount'] = $maxAmount;
                        $this->config[$env]['payin3']['numberOfPayments'] = 3;
                        break;

                    case 'pay-in-4':
                        $this->config[$env]['payin4']['minAmount'] = $minAmount;
                        $this->config[$env]['payin4']['maxAmount'] = $maxAmount;
                        $this->config[$env]['payin4']['numberOfPayments'] = 4;
                        break;
                }
            }

            // add this call to cache array
            $this->cache[$fingerprint] = $this->config[$env];
        }

        // return the configuration
        return $this->config;
    }

    /**
     * Returns the settings to call the API endpoints.
     *
     * @return array
     */
    private function getSettings(): array
    {
        // build and return settings array
        return [
            'test' => [
                'endpoint' => $this->scalapaySettings->getTestUrl(),
                'secretKey' => (string) $this->scalapaySettings->getTestApiKey()
            ],
            'production' => [
                'endpoint' => $this->scalapaySettings->getProductionUrl(),
                'secretKey' => (string) $this->scalapaySettings->getProductionApiKey()
            ]
        ];
    }
}
