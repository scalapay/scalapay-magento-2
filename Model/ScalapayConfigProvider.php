<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\Phrase;
use Scalapay\Scalapay\Gateway\Settings\Scalapay\Settings as ScalapaySettings;

/**
 * Class ScalapayConfigProvider
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Model
 */
class ScalapayConfigProvider implements ConfigProviderInterface
{

    /** @var ScalapaySettings $scalapaySettings */
    private $scalapaySettings;

    /**
     * ScalapayConfigProvider constructor.
     *
     * @param ScalapaySettings $scalapaySettings
     */
    public function __construct(
        ScalapaySettings $scalapaySettings
    ) {
        $this->scalapaySettings = $scalapaySettings;
    }

    /**
     * Returns Scalapay checkout template js configuration.
     *
     * @return array[][]
     */
    public function getConfig(): array
    {
        return [
            'payment' => [
                ScalapaySettings::CODE => [
                    'logo' => $this->getLogo(),
                    'title' => $this->getTitle()
                ]
            ]
        ];
    }

    /**
     * Returns the Scalapay payment title.
     *
     * @return Phrase
     */
    private function getTitle(): Phrase
    {
        return __('Pay with Scalapay [logo]');
    }

    /**
     * Returns the Scalapay black logo HTML.
     *
     * @return string
     */
    private function getLogo(): string
    {
        return '<span class="scalapay-checkout-widget-logo"></span>';
    }
}
