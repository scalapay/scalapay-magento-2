<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Model\ExtensionsData;

use Magento\Framework\App\ProductMetadataInterface;
use Magento\Framework\Module\ModuleList;

/**
 * Class PluginDetails
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Model\ExtensionData
 */
class PluginDetails
{
    /** @var string NOT_CUSTOMIZED */
    const NOT_CUSTOMIZED = '0';

    /** @var string PLATFORM_NAME */
    const PLATFORM_NAME = 'Magento';

    /** @var ProductMetadataInterface $productMetadataInterface */
    private $productMetadataInterface;

    /** @var ModuleList $moduleList */
    private $moduleList;

    /**
     * PluginDetails constructor.
     *
     * @param ProductMetadataInterface $productMetadataInterface
     * @param ModuleList $moduleList
     */
    public function __construct(
        ProductMetadataInterface $productMetadataInterface,
        ModuleList $moduleList
    ) {
        $this->productMetadataInterface = $productMetadataInterface;
        $this->moduleList = $moduleList;
    }

    /**
     * Returns plugin details array.
     *
     * @return array
     */
    public function getPluginDetailsData(): array
    {
        // get platform version
        $platformVersion = $this->productMetadataInterface->getVersion();

        // get plugin version
        $scalapayModule = $this->moduleList->getOne('Scalapay_Scalapay');
        $pluginVersion = $scalapayModule['setup_version'];

        // return plugin details array
        return [
            'platform' => self::PLATFORM_NAME,
            'platformVersion' => $platformVersion,
            'pluginVersion' => $pluginVersion,
            'customized' => self::NOT_CUSTOMIZED
        ];
    }
}
