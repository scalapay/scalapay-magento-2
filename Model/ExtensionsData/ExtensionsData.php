<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Model\ExtensionsData;

use Exception;
use Magento\Framework\App\ResourceConnection;
use Magento\Quote\Api\Data\CartInterface;
use Scalapay\Scalapay\Logger\Logger;

/**
 * Class ExtensionsData
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Model
 */
abstract class ExtensionsData
{
    /** @var string QUOTE_ADDRESS_TABLE */
    const SALES_ORDER_ADDRESS_TABLE = 'sales_order_address';

    /** @var string SALES_CREDITMEMO_TABLE */
    const SALES_CREDITMEMO_TABLE = 'sales_creditmemo';

    /** @var string SALES_ORDER_ADDRESS_TABLE */
    const SALES_ORDER_PAYMENT_TABLE = 'sales_order_payment';

    /** @var string SALES_ORDER_TABLE */
    const SALES_ORDER_TABLE = 'sales_order';

    /** @var ResourceConnection $resourceConnection */
    private $resourceConnection;

    /** @var Logger $logger */
    private $logger;

    /**
     * ExtensionsData constructor.
     *
     * @param ResourceConnection $resourceConnection
     * @param Logger $logger
     */
    public function __construct(
        ResourceConnection $resourceConnection,
        Logger $logger
    ) {
        $this->resourceConnection = $resourceConnection;
        $this->logger = $logger;
    }

    /**
     * Returns the order data of the customer.
     *
     * @param CartInterface $quote
     * @return array
     */
    protected function getCustomerOrders(CartInterface $quote): array
    {
        try {
            // get customer email
            $customerEmail = $this->getCustomerEmail($quote);

            // get connection
            $connection = $this->resourceConnection->getConnection();

            // extract orders data by customer email
            $salesOrderTableName = $this->resourceConnection->getTableName(self::SALES_ORDER_TABLE);
            $query = $connection->select()
                ->from($salesOrderTableName, [
                    'entity_id',
                    'grand_total',
                    'base_total_invoiced',
                    'base_total_refunded',
                    'order_currency_code',
                    'created_at'
                ])
                ->where('customer_email = ?', $customerEmail)
                ->where('created_at IS NOT NULL')
                ->order('created_at ASC');

            // return orders data
            return $connection->fetchAll($query);
        } catch (Exception $e) {
            // log exception
            $this->logger->critical(
                __CLASS__ .
                'Impossible retrieve customer orders ' .
                '(Quote ID: ' . $quote->getId() . '): ' .
                $e->getMessage()
            );

            // return empty array
            return [];
        }
    }

    /**
     * Returns the customer orders considered successful when they are invoiced.
     *
     * @param array $customerOrders
     * @return array
     */
    protected function getCustomerSuccessOrders(array $customerOrders): array
    {
        return array_filter($customerOrders, function ($customerOrder) {
            return $customerOrder['base_total_invoiced'] !== null;
        });
    }

    /**
     * Returns the customer orders refunded.
     *
     * @param array $customerOrders
     * @return array
     */
    protected function getRefundedCustomerOrders(array $customerOrders): array
    {
        try {
            // return empty array if there are no order ids
            $orderIds = $this->getOrderIds($customerOrders);
            if (!count($orderIds)) {
                return [];
            }

            // get connection
            $connection = $this->resourceConnection->getConnection();

            // extract refunded orders data by customer order ids
            $creditMemoTableName = $this->resourceConnection->getTableName(self::SALES_CREDITMEMO_TABLE);
            $query = $connection->select()
                ->from($creditMemoTableName, [
                    'entity_id',
                    'grand_total',
                    'order_currency_code',
                    'created_at'
                ])
                ->where('order_id IN (?)', $orderIds);

            // return refunded orders data
            return $connection->fetchAll($query);
        } catch (Exception $e) {
            // log exception
            $this->logger->critical(
                __CLASS__ .
                'Impossible retrieve customer refund orders: ' .
                $e->getMessage()
            );

            // return empty array
            return [];
        }
    }

    /**
     * Returns the customer order payments.
     *
     * @param array $customerSuccessOrders
     * @return array
     */
    protected function getCustomerOrderPayments(array $customerSuccessOrders): array
    {
        try {
            // return empty array if there are no order ids
            $orderIds = $this->getOrderIds($customerSuccessOrders);
            if (!count($orderIds)) {
                return [];
            }

            // get connection
            $connection = $this->resourceConnection->getConnection();

            // extract order payments data by customer order ids
            $salesOrderPaymentTableName = $this->resourceConnection->getTableName(self::SALES_ORDER_PAYMENT_TABLE);
            $query = $connection->select()
                ->from($salesOrderPaymentTableName, ['method'])
                ->where('parent_id IN (?)', $orderIds)
                ->distinct();

            // return order payments data
            return $connection->fetchAll($query);
        } catch (Exception $e) {
            // log exception
            $this->logger->critical(
                __CLASS__ .
                'Impossible retrieve customer order payments: ' .
                $e->getMessage()
            );

            // return empty array
            return [];
        }
    }

    /**
     * Returns the customer order shipping addresses.
     *
     * @param CartInterface $quote
     * @param array $customerSuccessOrders
     * @return array
     */
    protected function getCustomerOrderShippingAddresses(
        CartInterface $quote,
        array $customerSuccessOrders
    ): array {
        try {
            // return empty array if there are no order ids
            $orderIds = $this->getOrderIds($customerSuccessOrders);
            if (!count($orderIds)) {
                return [];
            }

            // get customer email
            $customerEmail = $this->getCustomerEmail($quote);

            // get connection
            $connection = $this->resourceConnection->getConnection();

            // extract customer shipping address data by customer email
            $salesOrderAddressTableName = $this->resourceConnection->getTableName(self::SALES_ORDER_ADDRESS_TABLE);
            $query = $connection->select()
                ->from($salesOrderAddressTableName, [
                    'entity_id',
                    'parent_id',
                    'city',
                    'street',
                    'region',
                    'postcode',
                    'country_id'
                ])
                ->where('parent_id IN (?)', $orderIds)
                ->where('email = ?', $customerEmail)
                ->where('address_type = ?', 'shipping');

            // return shipping address data
            return array_map(function ($address) {
                $address['uniqueKey'] = $this->clearStrings($address['city'] ?? '') .
                    $this->clearStrings($address['street'] ?? '') .
                    $this->clearStrings($address['postcode'] ?? '') .
                    $this->clearStrings($address['country_id'] ?? '');
                return $address;
            }, $connection->fetchAll($query));
        } catch (Exception $e) {
            // log exception
            $this->logger->critical(
                __CLASS__ .
                'Impossible retrieve customer order shipping addresses ' .
                '(Quote ID: ' . $quote->getId() . '): ' .
                $e->getMessage()
            );

            // return empty array
            return [];
        }
    }

    /**
     * Returns the order shipping address quote matches.
     *
     * @param CartInterface $quote
     * @param array $customerOrderShippingAddresses
     * @return array
     */
    protected function getOrderShippingAddressQuoteMatches(
        CartInterface $quote,
        array $customerOrderShippingAddresses
    ): array {
        try {
            // init vars
            $orderShippingAddressQuoteMatches = [];

            // return empty array if there are no customer order shipping addresses
            if (!count($customerOrderShippingAddresses)) {
                return $orderShippingAddressQuoteMatches;
            }

            // get quote data
            $city = $quote->getShippingAddress()->getCity() ?? '';
            $street = $quote->getShippingAddress()->getStreet() ?? '';
            $postcode = $quote->getShippingAddress()->getPostcode() ?? '';
            $countryId = $quote->getShippingAddress()->getCountryId() ?? '';

            // create quote address unique key
            $quoteAddressUniqueKey = $this->clearStrings($city) .
                $this->clearStrings($street) .
                $this->clearStrings($postcode) .
                $this->clearStrings($countryId);

            // extract order shipping address that matches with the current quote address
            foreach ($customerOrderShippingAddresses as $customerOrderShippingAddress) {
                if ($customerOrderShippingAddress['uniqueKey'] === $quoteAddressUniqueKey) {
                    $orderShippingAddressQuoteMatches[] = $customerOrderShippingAddress;
                }
            }

            // return order shipping address quote matches
            return $orderShippingAddressQuoteMatches;
        } catch (Exception $e) {
            // log exception
            $this->logger->critical(
                __CLASS__ .
                'Impossible retrieve order shipping address quote matches ' .
                '(Quote ID: ' . $quote->getId() . '): ' .
                $e->getMessage()
            );

            // return empty array
            return [];
        }
    }

    /**
     * Returns the customer email.
     *
     * @param CartInterface $quote
     * @return string
     * @throws Exception
     */
    protected function getCustomerEmail(CartInterface $quote): string
    {
        // get customer email
        $email = $quote->getCustomer()->getEmail() ??
            $quote->getBillingAddress()->getEmail() ??
            $quote->getShippingAddress()->getEmail() ??
            false;

        // throw exception if the customer email is missing
        if (!$email) {
            throw new Exception('Impossible retrieve the customer email.');
        }

        // return customer email
        return $email;
    }

    /**
     * Returns the order ids of the given orders.
     *
     * @param array $orders
     * @return array
     */
    protected function getOrderIds(array $orders): array
    {
        return array_map(
            function ($order) {
                return $order['entity_id'];
            },
            $orders
        );
    }

    /**
     * Formats the given date.
     *
     * @param string $date
     * @return string
     */
    protected function formatDate(string $date): string
    {
        return $date ? date('Y-m-d H:i:s', strtotime($date)) : '';
    }

    /**
     * Returns the given strings removing spaces and special characters.
     *
     * @param $strings
     * @return string
     */
    protected function clearStrings($strings): string
    {
        return is_array($strings) ?
            strtolower(preg_replace('/[\s\W]+/', '', implode(' ', $strings))) :
            strtolower(preg_replace('/[\s\W]+/', '', $strings));
    }
}
