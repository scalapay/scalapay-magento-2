<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Model\ExtensionsData;

use Magento\Framework\App\ResourceConnection;
use Magento\Quote\Api\Data\CartInterface;
use Scalapay\Scalapay\Gateway\Settings\Scalapay\Settings as ScalapaySettings;
use Scalapay\Scalapay\Helper\Payment as PaymentHelper;
use Scalapay\Scalapay\Logger\Logger;

/**
 * Class Risk
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Model\ExtensionsData
 */
class Risk extends ExtensionsData
{
    /** @var ScalapaySettings $scalapaySettings */
    private $scalapaySettings;

    /** @var PaymentHelper $paymentHelper */
    private $paymentHelper;

    /**
     * Risk constructor.
     *
     * @param ResourceConnection $resourceConnection
     * @param ScalapaySettings $scalapaySettings
     * @param PaymentHelper $paymentHelper
     * @param Logger $logger
     */
    public function __construct(
        ResourceConnection $resourceConnection,
        ScalapaySettings $scalapaySettings,
        PaymentHelper $paymentHelper,
        Logger $logger
    ) {
        parent::__construct($resourceConnection, $logger);
        $this->scalapaySettings = $scalapaySettings;
        $this->paymentHelper = $paymentHelper;
    }

    /**
     * Returns risk array.
     *
     * @param CartInterface $quote
     * @return array
     */
    public function getRiskData(CartInterface $quote): array
    {
        // exit if it is not a Scalapay payment method
        $paymentMethod = $quote->getPayment()->getMethod();
        if (!$this->paymentHelper->isScalapayPayment($paymentMethod)) {
            return [];
        }

        // exit if extra merchant data are disabled
        if (!$this->scalapaySettings->getEnableExtraMerchantData()) {
            return [];
        }

        // get customer orders and shipping addresses data
        $customerOrders = $this->getCustomerOrders($quote);
        $customerSuccessOrders = $this->getCustomerSuccessOrders($customerOrders);
        $refundedCustomerOrders = $this->getRefundedCustomerOrders($customerOrders);
        $customerOrderPayments = $this->getCustomerOrderPayments($customerSuccessOrders);
        $customerOrderShippingAddresses = $this->getCustomerOrderShippingAddresses($quote, $customerSuccessOrders);
        $orderShippingAddressQuoteMatches = $this->getOrderShippingAddressQuoteMatches($quote, $customerOrderShippingAddresses);

        // return risk array
        return [
            'returning' => $this->getReturning($customerSuccessOrders),
            'orderCountLifetime' => $this->getOrderCountLifetime($customerSuccessOrders),
            'orderCountL30d' => $this->getOrderCountL30d($customerSuccessOrders),
            'orderCountL180d' => $this->getOrderCountL180d($customerSuccessOrders),
            'orderCountLifetimeRefunded' => $this->getOrderCountLifetimeRefunded($refundedCustomerOrders),
            'orderCountL30dRefunded' => $this->getOrderCountL30dRefunded($refundedCustomerOrders),
            'orderAmountLifetimeEur' => $this->getOrderAmountLifetimeEur($customerSuccessOrders),
            'orderAmountL30dEur' => $this->getOrderAmountL30dEur($customerSuccessOrders),
            'orderAmountLifetimeRefundedEur' => $this->getOrderAmountLifetimeRefundedEur($refundedCustomerOrders),
            'orderAmountL30dRefundedEur' => $this->getOrderAmountL30dRefundedEur($refundedCustomerOrders),
            'lastOrderTimestamp' => $this->getLastOrderTimestamp($customerSuccessOrders),
            'lastRefundTimestamp' => $this->getLastRefundTimestamp($refundedCustomerOrders),
            'accountCreationTimestamp' => $this->getAccountCreationTimestamp($quote),
            'paymentMethodCount' => $this->getPaymentMethodCount($customerOrderPayments),
            'shippingAddressCount' => $this->getShippingAddressCount($customerOrderShippingAddresses),
            'shippingAddressUseCount' => $this->getShippingAddressUseCount($orderShippingAddressQuoteMatches),
            'shippingAddressTimestamp' => $this->getShippingAddressTimestamp(
                $quote,
                $orderShippingAddressQuoteMatches,
                $customerSuccessOrders
            )
        ];
    }

    /**
     * Returns true if the customer is returning else false.
     *
     * @param array $customerSuccessOrders
     * @return bool
     */
    protected function getReturning(array $customerSuccessOrders): bool
    {
        return count($customerSuccessOrders) > 0;
    }

    /**
     * Returns the customer order count.
     *
     * @param array $customerSuccessOrders
     * @return int
     */
    protected function getOrderCountLifetime(array $customerSuccessOrders): int
    {
        return count($customerSuccessOrders);
    }

    /**
     * Returns the customer order count in the last 30 days.
     *
     * @param array $customerSuccessOrders
     * @return int
     */
    protected function getOrderCountL30d(array $customerSuccessOrders): int
    {
        return count(
            array_filter(
                $customerSuccessOrders,
                function ($successCustomerOrder) {
                    return isset($successCustomerOrder['created_at']) &&
                        strtotime($successCustomerOrder['created_at']) > strtotime('-30 days');
                }
            )
        );
    }

    /**
     * Returns the customer order count in the last 180 days.
     *
     * @param array $customerSuccessOrders
     * @return int
     */
    protected function getOrderCountL180d(array $customerSuccessOrders): int
    {
        return count(
            array_filter(
                $customerSuccessOrders,
                function ($successCustomerOrder) {
                    return isset($successCustomerOrder['created_at']) &&
                        strtotime($successCustomerOrder['created_at']) > strtotime('-180 days');
                }
            )
        );
    }

    /**
     * Returns the customer order refunded count.
     *
     * @param array $refundedCustomerOrders
     * @return int
     */
    protected function getOrderCountLifetimeRefunded(array $refundedCustomerOrders): int
    {
        return count($refundedCustomerOrders);
    }

    /**
     * Returns customer order count in the last 30 days.
     *
     * @param array $refundedCustomerOrders
     * @return int
     */
    protected function getOrderCountL30dRefunded(array $refundedCustomerOrders): int
    {
        return count(
            array_filter(
                $refundedCustomerOrders,
                function ($refundedCustomerOrder) {
                    return isset($refundedCustomerOrder['created_at']) &&
                        strtotime($refundedCustomerOrder['created_at']) > strtotime('-30 days');
                }
            )
        );
    }

    /**
     * Returns the amount of all orders of the customer.
     *
     * @param array $customerSuccessOrders
     * @return float
     */
    protected function getOrderAmountLifetimeEur(array $customerSuccessOrders): float
    {
        // init vars
        $total = 0;

        // return if there are no customer success orders
        if (!count($customerSuccessOrders)) {
            return $total;
        }

        // calculate total
        foreach ($customerSuccessOrders as $order) {
            if (!isset($order['order_currency_code']) || $order['order_currency_code'] !== 'EUR') {
                continue;
            }

            $total += $order['grand_total'] ?? 0;
        }

        // return total
        return $total;
    }

    /**
     * Returns the amount of all orders of the customer in the last 30 days.
     *
     * @param array $customerSuccessOrders
     * @return float
     */
    protected function getOrderAmountL30dEur(array $customerSuccessOrders): float
    {
        // init vars
        $total = 0;

        // return if there are no customer success orders
        if (!count($customerSuccessOrders)) {
            return $total;
        }

        // calculate total
        foreach ($customerSuccessOrders as $order) {
            if (!isset($order['order_currency_code']) || $order['order_currency_code'] !== 'EUR') {
                continue;
            }

            if (!isset($order['created_at']) || strtotime($order['created_at']) < strtotime('-30 days')) {
                continue;
            }

            $total += $order['grand_total'] ?? 0;
        }

        // return total
        return $total;
    }

    /**
     * Returns the amount of all refunded orders of the customer.
     *
     * @param array $refundedCustomerOrders
     * @return float
     */
    protected function getOrderAmountLifetimeRefundedEur(array $refundedCustomerOrders): float
    {
        // init vars
        $total = 0;

        // return if there are no refund customer orders
        if (!count($refundedCustomerOrders)) {
            return $total;
        }

        // calculate total
        foreach ($refundedCustomerOrders as $order) {
            if (!isset($order['order_currency_code']) || $order['order_currency_code'] !== 'EUR') {
                continue;
            }

            $total += $order['grand_total'] ?? 0;
        }

        // return total
        return $total;
    }

    /**
     * Returns the amount of all refunded orders of the customer in the last 30 days.
     *
     * @param array $refundedCustomerOrders
     * @return float
     */
    protected function getOrderAmountL30dRefundedEur(array $refundedCustomerOrders): float
    {
        // init vars
        $total = 0;

        // return if there are no refund customer orders
        if (!count($refundedCustomerOrders)) {
            return $total;
        }

        // calculate total
        foreach ($refundedCustomerOrders as $order) {
            if (!isset($order['order_currency_code']) || $order['order_currency_code'] !== 'EUR') {
                continue;
            }

            if (!isset($order['created_at']) || strtotime($order['created_at']) < strtotime('-30 days')) {
                continue;
            }

            $total += $order['grand_total'] ?? 0;
        }

        // return total
        return $total;
    }

    /**
     * Returns the last order timestamp.
     *
     * @param array $customerSuccessOrders
     * @return string
     */
    protected function getLastOrderTimestamp(array $customerSuccessOrders): string
    {
        // return an empty string if there are no customer success orders
        if (!count($customerSuccessOrders)) {
            return '';
        }

        // return last order timestamp
        return $this->formatDate($customerSuccessOrders[count($customerSuccessOrders) - 1]['created_at'] ?? '');
    }

    /**
     * Returns the account creation timestamp.
     *
     * @param CartInterface $quote
     * @return string
     */
    protected function getAccountCreationTimestamp(CartInterface $quote): string
    {
        // return no timestamp if the user is a guest
        if (!$quote->getCustomerId()) {
            return '';
        }

        // return timestamp if the user is a registered customer
        $createdAt = $quote->getCustomer()->getCreatedAt() ?? '';
        return $this->formatDate($createdAt);
    }

    /**
     * Returns the last refund timestamp.
     *
     * @param array $refundedCustomerOrders
     * @return string
     */
    protected function getLastRefundTimestamp(array $refundedCustomerOrders): string
    {
        // return zero timestamp if there are no refunded customer orders
        if (!count($refundedCustomerOrders)) {
            return '';
        }

        // return last refund timestamp
        return $this->formatDate($refundedCustomerOrders[count($refundedCustomerOrders) - 1]['created_at'] ?? '');
    }

    /**
     * Returns the customer order payments count.
     *
     * @param array $customerOrderPayments
     * @return int
     */
    protected function getPaymentMethodCount(array $customerOrderPayments): int
    {
        return count($customerOrderPayments);
    }

    /**
     * Returns the customer shipping addresses count.
     *
     * @param array $customerOrderShippingAddresses
     * @return int
     */
    protected function getShippingAddressCount(array $customerOrderShippingAddresses): int
    {
        // return zero if there are no customer order shipping addresses
        if (!count($customerOrderShippingAddresses)) {
            return 0;
        }

        // init vars
        $uniqueAddresses = [];

        // individuate unique addresses
        foreach ($customerOrderShippingAddresses as $address) {
            // check if the address is unique based on the unique key
            if (!isset($uniqueAddresses[$address['uniqueKey']])) {
                $uniqueAddresses[$address['uniqueKey']] = $address;
            }
        }

        // return count
        return count($uniqueAddresses);
    }

    /**
     * Returns the current shipping address creation timestamp.
     *
     * @param CartInterface $quote
     * @param array $orderShippingAddressQuoteMatches
     * @param array $customerSuccessOrders
     * @return string
     */
    protected function getShippingAddressTimestamp(
        CartInterface $quote,
        array $orderShippingAddressQuoteMatches,
        array $customerSuccessOrders
    ): string {
        // return quote address creation timestamp if there are no customer success orders
        if (!count($customerSuccessOrders)) {
            return $this->formatDate($quote->getShippingAddress()->getCreatedAt());
        }

        // get order shipping address quote match ids
        $orderShippingAddressQuoteMatchIds = array_map(function ($orderShippingAddressQuoteMatch) {
            return $orderShippingAddressQuoteMatch['parent_id'];
        }, $orderShippingAddressQuoteMatches);

        // get order that matches the quote shipping address
        $orderMatches = array_filter($customerSuccessOrders, function ($order) use ($orderShippingAddressQuoteMatchIds) {
            return in_array($order['entity_id'], $orderShippingAddressQuoteMatchIds);
        });

        // get the first order
        $firstOrder = array_shift($orderMatches);

        // return the first order creation timestamp that matches the quote shipping address
        // else return the quote address creation timestamp
        return $firstOrder ?
            $this->formatDate($firstOrder['created_at']) :
            $this->formatDate($quote->getShippingAddress()->getCreatedAt());
    }

    /**
     * Returns the shipping address use count.
     *
     * @param array $orderShippingAddressQuoteMatches
     * @return int
     */
    protected function getShippingAddressUseCount(array $orderShippingAddressQuoteMatches): int
    {
        return count($orderShippingAddressQuoteMatches);
    }
}
