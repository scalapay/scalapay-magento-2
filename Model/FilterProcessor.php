<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Model;

use InvalidArgumentException;
use Magento\Quote\Api\Data\CartInterface;
use Scalapay\Scalapay\Model\Filters\Interfaces\FilterInterface;

/**
 * Class FilterProcessor
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Model
 */
class FilterProcessor
{
    /** @var array $filters */
    private $filters;

    /**
     * ProcessFilters constructor.
     *
     * @param array $filters
     */
    public function __construct(
        array $filters = []
    ) {
        // get and check filters
        $this->filters = $filters;
        foreach ($this->filters as $filterCode => $filter) {
            if (!($filter instanceof FilterInterface)) {
                throw new InvalidArgumentException(
                    'Filter ' . $filterCode . ' must implement ' . FilterInterface::class
                );
            }
        }
    }

    /**
     * Returns false if a filter fails else true.
     *
     * @param CartInterface $quote
     * @return bool
     */
    public function execute(CartInterface $quote): bool
    {
        /** @var FilterInterface $filter */
        foreach ($this->filters as $filter) {
            if (!$filter->execute($quote)) {
                return false;
            }
        }

        return true;
    }
}
