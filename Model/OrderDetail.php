<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Model;

use Exception;
use Magento\Catalog\Api\ProductRepositoryInterfaceFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\UrlInterface;
use Magento\Quote\Api\Data\CartInterface;
use Magento\Store\Model\StoreManagerInterface;
use Scalapay\Scalapay\Gateway\Settings\Scalapay\Settings as ScalapaySettings;
use Scalapay\Scalapay\Model\ExtensionsData\PluginDetails as PluginDetailsData;
use Scalapay\Scalapay\Model\ExtensionsData\Risk as RiskData;
use Scalapay\Sdk\Model\Customer\Consumer;
use Scalapay\Sdk\Model\Customer\Contact;
use Scalapay\Sdk\Model\Merchant\MerchantOptions;
use Scalapay\Sdk\Model\Order\OrderDetails;
use Scalapay\Sdk\Model\Order\OrderDetails\Discount;
use Scalapay\Sdk\Model\Order\OrderDetails\Extensions;
use Scalapay\Sdk\Model\Order\OrderDetails\Extensions\Industry;
use Scalapay\Sdk\Model\Order\OrderDetails\Extensions\PluginDetails;
use Scalapay\Sdk\Model\Order\OrderDetails\Extensions\Risk;
use Scalapay\Sdk\Model\Order\OrderDetails\Extensions\Type;
use Scalapay\Sdk\Model\Order\OrderDetails\Item;
use Scalapay\Sdk\Model\Order\OrderDetails\Money;

/**
 * Class OrderDetail
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Model
 */
class OrderDetail
{
    /** @var string TYPE_ONLINE */
    const TYPE_ONLINE = 'online';

    /** @var ProductRepositoryInterfaceFactory $productRepositoryInterfaceFactory */
    private $productRepositoryInterfaceFactory;

    /** @var StoreManagerInterface $storeManagerInterface */
    private $storeManagerInterface;

    /** @var PluginDetailsData $pluginDetailsData */
    private $pluginDetailsData;

    /** @var RiskData $riskData */
    private $riskData;

    /**
     * GetOrderDetails constructor.
     *
     * @param ProductRepositoryInterfaceFactory $productRepositoryInterfaceFactory
     * @param StoreManagerInterface $storeManager
     * @param PluginDetailsData $pluginDetailsData
     * @param RiskData $riskData
     */
    public function __construct(
        ProductRepositoryInterfaceFactory $productRepositoryInterfaceFactory,
        StoreManagerInterface $storeManager,
        PluginDetailsData $pluginDetailsData,
        RiskData $riskData
    ) {
        $this->productRepositoryInterfaceFactory = $productRepositoryInterfaceFactory;
        $this->storeManagerInterface = $storeManager;
        $this->pluginDetailsData = $pluginDetailsData;
        $this->riskData = $riskData;
    }

    /**
     * Returns order details object.
     *
     * @param CartInterface $quote
     * @return OrderDetails
     * @throws NoSuchEntityException
     * @throws Exception
     */
    public function execute(
        CartInterface $quote
    ): OrderDetails {
        // set total amount
        $totalAmount = new Money();
        $totalAmount->setAmount((float) $quote->getGrandTotal());

        // set customer
        $consumer = new Consumer();
        $consumer->setEmail($quote->getBillingAddress()->getEmail())
            ->setGivenNames($quote->getBillingAddress()->getFirstname())
            ->setSurname($quote->getBillingAddress()->getLastname())
            ->setPhoneNumber($quote->getBillingAddress()->getTelephone());

        // set billing address
        $billingAddress = new Contact();
        $billingAddress->setName($quote->getBillingAddress()->getName())
            ->setLine1($quote->getBillingAddress()->getStreetLine(1))
            ->setLine2($quote->getBillingAddress()->getStreetLine(2))
            ->setPostcode($quote->getBillingAddress()->getPostcode())
            ->setSuburb($quote->getBillingAddress()->getCity())
            ->setCountryCode($quote->getBillingAddress()->getCountryId())
            ->setPhoneNumber($quote->getBillingAddress()->getTelephone());

        // set shipping address
        $shippingAddress = new Contact();
        $shippingAddress->setName($quote->getShippingAddress()->getName())
            ->setLine1($quote->getShippingAddress()->getStreetLine(1))
            ->setLine2($quote->getShippingAddress()->getStreetLine(2))
            ->setPostcode($quote->getShippingAddress()->getPostcode())
            ->setSuburb($quote->getShippingAddress()->getCity())
            ->setCountryCode($quote->getShippingAddress()->getCountryId())
            ->setPhoneNumber($quote->getShippingAddress()->getTelephone());

        // set items
        $items = [];
        $taxTotal = 0;
        foreach ($quote->getAllVisibleItems() as $quoteItem) {
            // build item object
            $item = new Item();
            $item->setName($quoteItem->getName());
            $item->setSku($quoteItem->getSku());
            $item->setQuantity($quoteItem->getQty());
            $item->setPageUrl($quoteItem->getProduct()->getProductUrl());
            $item->setImageUrl($this->getProductThumbnailUrl($quoteItem->getProduct()->getId()));
            $itemPrice = new Money();
            $itemPrice->setAmount((float) $quoteItem->getPrice());
            $item->setPrice($itemPrice);
            $items[] = $item;

            // get tax total
            $taxTotal += (float) $quoteItem->getTaxAmount();
        }

        // set discounts
        $discountAmount = new Money();
        $discountAmount->setAmount((float) $quote->getSubtotal() - (float) $quote->getSubtotalWithDiscount());
        $discount = new Discount();
        $discount->setDisplayName('Total discount');
        $discount->setAmount($discountAmount);
        $discountList[] = $discount;

        // set tax amount
        $taxAmount = new Money();
        $taxAmount->setAmount((float) $taxTotal);

        // set shipping amount
        $shippingAmount = new Money();
        $shippingAmount->setAmount((float) $quote->getShippingAddress()->getShippingAmount());

        // get confirm and cancel url
        $confirmUrl = $this->storeManagerInterface->getStore()->getUrl('scalapay/confirm');
        $cancelUrl = $this->storeManagerInterface->getStore()->getUrl('scalapay/cancel');

        // set merchant options
        $merchantOptions = new MerchantOptions();
        $merchantOptions->setRedirectConfirmUrl($confirmUrl);
        $merchantOptions->setRedirectCancelUrl($cancelUrl);

        // set merchant reference
        $merchantReference = $quote->getReservedOrderId();

        // set extensions
        $pluginDetails = $this->getPluginDetails();
        $risk = $this->getRisk($quote);
        $extensions = $this->getExtensions($pluginDetails, $risk);

        // set and return order details
        $orderDetails = new OrderDetails();
        return $orderDetails->setTotalAmount($totalAmount)
            ->setConsumer($consumer)
            ->setBilling($billingAddress)
            ->setShipping($shippingAddress)
            ->setItems($items)
            ->setDiscounts($discountList)
            ->setTaxAmount($taxAmount)
            ->setShippingAmount($shippingAmount)
            ->setMerchant($merchantOptions)
            ->setMerchantReference($merchantReference)
            ->setType(self::TYPE_ONLINE)
            ->setExtensions($extensions);
    }

    /**
     * Returns the product thumbnail url.
     *
     * @param $productId
     * @return string
     */
    private function getProductThumbnailUrl($productId): string
    {
        try {
            $product = $this->productRepositoryInterfaceFactory->create()->getById($productId);
            return $this->storeManagerInterface->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA) .
                'catalog/product' .
                $product->getThumbnail();
        } catch (Exception $e) {
            return '';
        }
    }

    /**
     * Returns extensions object.
     *
     * @param Risk|null $risk
     * @param Industry|null $industry
     * @param Type|null $type
     * @param PluginDetails|null $pluginDetails
     * @return Extensions
     */
    private function getExtensions(
        ?PluginDetails $pluginDetails = null,
        ?Risk $risk = null,
        ?Industry $industry = null,
        ?Type $type = null
    ): Extensions {
        $extensions = new Extensions();
        return $extensions->setPluginDetails($pluginDetails)
            ->setRisk($risk)
            ->setIndustry($industry)
            ->setType($type);
    }

    /**
     * Returns risk object.
     *
     * @param CartInterface $quote
     * @return Risk
     */
    private function getRisk(CartInterface $quote): Risk
    {
        // get risk data
        $riskData = $this->riskData->getRiskData($quote);

        // set and return risk object
        $risk = new Risk();
        return !count($riskData) ? $risk : $risk->setReturning($riskData['returning'])
            ->setOrderCountLifetime($riskData['orderCountLifetime'])
            ->setOrderCountL30d($riskData['orderCountL30d'])
            ->setOrderCountL180d($riskData['orderCountL180d'])
            ->setOrderCountLifetimeRefunded($riskData['orderCountLifetimeRefunded'])
            ->setOrderCountL30dRefunded($riskData['orderCountL30dRefunded'])
            ->setOrderAmountLifetimeEur($riskData['orderAmountLifetimeEur'])
            ->setOrderAmountL30dEur($riskData['orderAmountL30dEur'])
            ->setOrderAmountLifetimeRefundedEur($riskData['orderAmountLifetimeRefundedEur'])
            ->setOrderAmountL30dRefundedEur($riskData['orderAmountL30dRefundedEur'])
            ->setLastOrderTimestamp($riskData['lastOrderTimestamp'])
            ->setLastRefundTimestamp($riskData['lastRefundTimestamp'])
            ->setAccountCreationTimestamp($riskData['accountCreationTimestamp'])
            ->setPaymentMethodCount($riskData['paymentMethodCount'])
            ->setShippingAddressCount($riskData['shippingAddressCount'])
            ->setShippingAddressTimestamp($riskData['shippingAddressTimestamp'])
            ->setShippingAddressUseCount($riskData['shippingAddressUseCount']);
    }

    /**
     * Returns plugin details object.
     *
     * @return PluginDetails
     */
    private function getPluginDetails(): PluginDetails
    {
        // get plugin details data
        $pluginDetailsData = $this->pluginDetailsData->getPluginDetailsData();

        // set and return plugin details object
        $pluginDetails = new PluginDetails();
        return $pluginDetails->setPlatform($pluginDetailsData['platform'])
            ->setPlatformVersion($pluginDetailsData['platformVersion'])
            ->setPluginVersion($pluginDetailsData['pluginVersion'])
            ->setCustomized($pluginDetailsData['customized']);
    }
}
