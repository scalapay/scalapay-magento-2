<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Model;

use Exception;

/**
 * Class ApiPayment
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Model
 */
class ApiPayment
{
    /** @var ApiClient $apiClient */
    private $apiClient;

    /**
     * ApiPayment constructor.
     *
     * @param ApiClient $apiClient
     */
    public function __construct(ApiClient $apiClient)
    {
        $this->apiClient = $apiClient;
    }

    /**
     * Returns the Scalapay API payment.
     *
     * @param string $orderToken
     * @param string $paymentMethod
     * @param int|null $storeId
     * @return array
     * @throws Exception
     */
    public function get(
        string $orderToken,
        string $paymentMethod,
        ?int $storeId = null
    ): array {
        // check data
        if (!$orderToken) {
            throw new Exception('Payment info API - Critical error: Invalid order token.');
        }

        if (!$paymentMethod) {
            throw new Exception('Payment info API - Critical error: Invalid payment method.');
        }

        // get api client
        $apiClient = $this->apiClient->execute($paymentMethod, $storeId);

        // get payment info
        $response = $apiClient->getPaymentInfo($orderToken);

        // return response
        return $response['body'];
    }
}
