# Magento 2 - Scalapay Payment Module
- [Introduction](#markdown-header-introduction)
- [System Requirements](#markdown-header-system-requirements)
- [Installation](#markdown-header-installation)
- [Configuration](#markdown-header-configuration)
- [Additional Information](#markdown-header-additional-information)

## Introduction
Scalapay is an innovative payment method that allows customers to pay in 3 or 4 comfortable installments on a monthly basis.<br>
This plugin permits you to configure easily the payment gateway and display Scalapay in your product pages, cart pages and checkout.

## System Requirements
* Magento 2.2.2 or greater
* PHP 7.1 or greater

## Installation
- Login via SSH into your server
- Go to the Magento root
- Enable maintenance mode by running `php bin/magento maintenance:enable`
- Download the module by running `composer require scalapay/scalapay-magento-2`
- Enable the module by running `php bin/magento module:enable Scalapay_Scalapay`
- Apply database updates by running `php bin/magento setup:upgrade`
- Deploy the static content by running `php bin/magento setup:static-content:deploy`
- Compile the source by running `php bin/magento setup:di:compile`
- Clear the cache by running `php bin/magento cache:clean`
- Disable maintenance mode by running `php bin/magento maintenance:disable`

## Configuration
- Stores -> Configuration -> Sales -> Scalapay
- Stores -> Configuration -> Sales -> Payment Methods :: Scalapay

## Additional Information
[View the Complete User Guide](https://developers.scalapay.com/docs/pluginstall-magento2)
