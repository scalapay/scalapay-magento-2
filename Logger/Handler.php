<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Logger;

use Magento\Framework\Filesystem\DriverInterface;
use Magento\Framework\Logger\Handler\Base as BaseHandler;
use Monolog\Logger as MonologLogger;

/**
 * Class Handler
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Logger
 */
class Handler extends BaseHandler
{
    /** @var int $loggerType */
    protected $loggerType = MonologLogger::INFO;

    /** @var string $fileName */
    protected $fileName = '/var/log/scalapay_{{month}}_{{year}}.log';

    /**
     * Handler constructor.
     *
     * @param DriverInterface $filesystem
     * @param string $filePath
     */
    public function __construct(
        DriverInterface $filesystem,
        $filePath = null
    ) {
        $this->fileName = str_replace('{{month}}', date('m'), $this->fileName);
        $this->fileName = str_replace('{{year}}', date('Y'), $this->fileName);
        parent::__construct($filesystem, $filePath, $this->fileName);
    }
}
