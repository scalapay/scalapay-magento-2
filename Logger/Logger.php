<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Logger;

use Monolog\Logger as MonologLogger;

/**
 * Class Logger
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Logger
 */
class Logger extends MonologLogger
{
}
