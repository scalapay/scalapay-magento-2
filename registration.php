<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Scalapay_Scalapay',
    __DIR__
);
