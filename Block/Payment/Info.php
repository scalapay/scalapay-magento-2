<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Block\Payment;

use Magento\Framework\DataObject;
use Magento\Framework\Exception\LocalizedException;
use Magento\Payment\Block\ConfigurableInfo;

/**
 * Class Info
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Block\Payment
 */
class Info extends ConfigurableInfo
{
    /**
     * Prepares payment information adding Scalapay data.
     *
     * @param $transport
     * @return DataObject
     * @throws LocalizedException
     */
    protected function _prepareSpecificInformation($transport = null): DataObject
    {
        $transport = parent::_prepareSpecificInformation($transport);
        $payment = $this->getInfo();

        return $transport->addData([
            'Transaction ID' => $payment->getOrder()->getScalapayOrderToken()
        ]);
    }
}
