<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Block\Adminhtml\Config\Field;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;

/**
 * Class Disabled
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Block\Adminhtml\Config\Field
 */
class Disabled extends Field
{
    /**
     * Adds 'disable' and 'readonly' attributes in the input field HTML DOM.
     *
     * @param AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(AbstractElement $element): string
    {
        /** @noinspection PhpUndefinedMethodInspection */
        $element->setDisabled('disabled');
        $element->setReadonly(true);

        return $element->getElementHtml();
    }
}
