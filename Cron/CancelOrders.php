<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Cron;

use Exception;
use Magento\Framework\App\ResourceConnection;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Api\OrderStatusHistoryRepositoryInterface;
use Magento\Sales\Model\ResourceModel\Order\Collection;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory as OrderCollectionFactory;
use Scalapay\Scalapay\Gateway\Settings\Scalapay\Settings as ScalapaySettings;
use Scalapay\Scalapay\Helper\Config as ConfigHelper;
use Scalapay\Scalapay\Helper\Order as OrderHelper;
use Scalapay\Scalapay\Logger\Logger;
use Scalapay\Scalapay\Model\ApiPayment;

/**
 * Class CancelOrders
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Cron
 */
class CancelOrders
{
    /** @var string EXPIRED_ORDER_COMMENT */
    const EXPIRED_ORDER_COMMENT = 'Scalapay: Payment authorize has been expired';

    /** @var string[] MAGENTO_AUTHORIZE_STATUS_NOT_PROCESSABLE */
    const MAGENTO_AUTHORIZE_STATUS_NOT_PROCESSABLE = ['processing', 'fraud', 'complete', 'closed', 'canceled', 'shipped'];

    /** @var string[] MAGENTO_INVALID_STATUS_NOT_PROCESSABLE */
    const MAGENTO_INVALID_STATUS_NOT_PROCESSABLE = ['processing', 'fraud', 'complete', 'closed', 'shipped'];

    /** @var string SALES_ORDER_PAYMENT_TABLE */
    const SALES_ORDER_PAYMENT_TABLE = 'sales_order_payment';

    /** @var string[] SCALAPAY_BAD_STATUS */
    const SCALAPAY_BAD_STATUS = ['expired', 'refunded_not_charged'];

    /** @var ResourceConnection $resourceConnection */
    private $resourceConnection;

    /** @var OrderRepositoryInterface $orderRepositoryInterface */
    private $orderRepositoryInterface;

    /** @var OrderStatusHistoryRepositoryInterface $orderStatusHistoryRepositoryInterface */
    private $orderStatusHistoryRepositoryInterface;

    /** @var OrderCollectionFactory $orderCollectionFactory */
    private $orderCollectionFactory;

    /** @var ScalapaySettings $scalapaySettings */
    private $scalapaySettings;

    /** @var ConfigHelper $configHelper */
    private $configHelper;

    /** @var OrderHelper $orderHelper */
    private $orderHelper;

    /** @var Logger $logger */
    private $logger;

    /** @var ApiPayment $apiPayment */
    private $apiPayment;

    /**
     * CancelOrders constructor.
     *
     * @param ResourceConnection $resourceConnection
     * @param OrderRepositoryInterface $orderRepositoryInterface
     * @param OrderStatusHistoryRepositoryInterface $orderStatusHistoryRepositoryInterface
     * @param OrderCollectionFactory $orderCollectionFactory
     * @param ScalapaySettings $scalapaySettings
     * @param ConfigHelper $configHelper
     * @param OrderHelper $orderHelper
     * @param Logger $logger
     * @param ApiPayment $apiPayment
     */
    public function __construct(
        ResourceConnection $resourceConnection,
        OrderRepositoryInterface $orderRepositoryInterface,
        OrderStatusHistoryRepositoryInterface $orderStatusHistoryRepositoryInterface,
        OrderCollectionFactory $orderCollectionFactory,
        ScalapaySettings $scalapaySettings,
        ConfigHelper $configHelper,
        OrderHelper $orderHelper,
        Logger $logger,
        ApiPayment $apiPayment
    ) {
        $this->resourceConnection = $resourceConnection;
        $this->orderRepositoryInterface = $orderRepositoryInterface;
        $this->orderStatusHistoryRepositoryInterface = $orderStatusHistoryRepositoryInterface;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->scalapaySettings = $scalapaySettings;
        $this->configHelper = $configHelper;
        $this->orderHelper = $orderHelper;
        $this->logger = $logger;
        $this->apiPayment = $apiPayment;
    }

    /**
     * Moves the order with expired authorization on invalid status.
     *
     * @return bool
     */
    public function execute(): bool
    {
        try {
            // get Scalapay orders in authorize status (pending orders)
            $scalapayOrdersInAuthorizeStatus = $this->getScalapayOrdersInAuthorizeStatus();

            // exit if there are no Scalapay pending orders
            if (!$scalapayOrdersInAuthorizeStatus->count()) {
                return false;
            }

            // loop orders
            $scalapayOrdersInAuthorizeStatus = $scalapayOrdersInAuthorizeStatus->getData();
            foreach ($scalapayOrdersInAuthorizeStatus as $scalapayOrder) {
                try {
                    // get and check order data
                    $orderId = $scalapayOrder['entity_id'] ?? null;
                    $orderToken = $scalapayOrder['scalapay_order_token'] ?? null;
                    $orderStoreId = $scalapayOrder['store_id'] ?? null;
                    $paymentMethod = $scalapayOrder['method'] ?? null;
                    if (!$orderId || !$orderToken || !$orderStoreId || !$paymentMethod) {
                        continue;
                    }

                    // throw silent exception if authorize status or invalid status are not allowed
                    $this->validateStatuses();

                    // call Scalapay payment API
                    $paymentInfo = $this->apiPayment->get($orderToken, $paymentMethod, $orderStoreId);

                    // check payment status on Scalapay side
                    if (!isset($paymentInfo['status']) ||
                        !in_array($paymentInfo['status'], self::SCALAPAY_BAD_STATUS)
                    ) {
                        continue;
                    }

                    // move to invalid order status the expired payments
                    // get invalid order status
                    $invalidStatus = $this->scalapaySettings->getInvalidOrderStatus();
                    $invalidState = $this->orderHelper->getStateByStatus($invalidStatus);

                    // update order state and status if the payment authorize is expired
                    $order = $this->orderRepositoryInterface->get($orderId);
                    $order->setState($invalidState)->setStatus($invalidStatus);
                    $this->orderRepositoryInterface->save($order);

                    // add comment if the payment authorize is expired
                    $comment = $order->addCommentToStatusHistory(self::EXPIRED_ORDER_COMMENT, $invalidStatus);
                    $this->orderStatusHistoryRepositoryInterface->save($comment);
                } catch (Exception $e) {
                    // silent catch - skip to the next order in case of exception
                    continue;
                }
            }
        } catch (Exception $e) {
            // log exception
            $this->logger->critical(__CLASS__ . ': ' . $e->getMessage());

            // return
            return false;
        }

        // return
        return true;
    }

    /**
     * Returns the Scalapay orders in authorize status.
     *
     * @return Collection
     * @throws Exception
     */
    private function getScalapayOrdersInAuthorizeStatus(): Collection
    {
        // get order collection
        $orderCollection = $this->orderCollectionFactory->create();

        // get sales order payment table name considering an eventual prefix
        $salesOrderPaymentTable = $this->resourceConnection->getTableName(self::SALES_ORDER_PAYMENT_TABLE);

        // init join query
        $joinQuery = $orderCollection->getSelect()
            ->join(
                ['sop' => $salesOrderPaymentTable],
                'main_table.entity_id = sop.parent_id',
                ['method']
            );

        // get authorize status for current payment method
        $authorizeStatus = $this->scalapaySettings->getAuthorizeOrderStatus();
        if (!$authorizeStatus) {
            throw new Exception('Authorize order status is empty.');
        }

        // add or where condition mapped between payment method and authorize status
        $joinQuery->where(
            'main_table.status = "' . $authorizeStatus . '" AND sop.method = "' . ScalapaySettings::CODE . '"'
        );

        // return collection
        return $orderCollection->load();
    }

    /**
     * Validates authorize and invalid status configurations.
     *
     * @return void
     * @throws Exception
     */
    private function validateStatuses(): void
    {
        // get authorize status and invalid status
        $authorizeOrderStatus = $this->scalapaySettings->getAuthorizeOrderStatus();
        $invalidOrderStatus = $this->scalapaySettings->getInvalidOrderStatus();

        // check if authorize order status config is allowed
        if (!$authorizeOrderStatus) {
            throw new Exception('Authorize order status is empty.');
        }

        if (in_array($authorizeOrderStatus, self::MAGENTO_AUTHORIZE_STATUS_NOT_PROCESSABLE)) {
            throw new Exception('Authorize order status is not processable.');
        }

        // check if invalid order status config is allowed
        if (!$invalidOrderStatus) {
            throw new Exception('Invalid order status is empty.');
        }

        if (in_array($invalidOrderStatus, self::MAGENTO_INVALID_STATUS_NOT_PROCESSABLE)) {
            throw new Exception('Invalid order status is not processable.');
        }
    }
}
