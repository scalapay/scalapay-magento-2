<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Cron;

use Exception;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem\Driver\File as FileDriver;
use Scalapay\Scalapay\Logger\Logger;

/**
 * Class LogCleaner
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Cron
 */
class LogCleaner
{
    /** @var string SCALAPAY_LOG_PREFIX */
    const SCALAPAY_LOG_PREFIX = 'scalapay_';

    /** @var DirectoryList $directoryList */
    private $directoryList;

    /** @var FileDriver $fileDriver */
    private $fileDriver;

    /** @var Logger $logger */
    private $logger;

    /**
     * LogCleaner constructor.
     *
     * @param DirectoryList $directoryList
     * @param FileDriver $fileDriver
     * @param Logger $logger
     */
    public function __construct(
        DirectoryList $directoryList,
        FileDriver $fileDriver,
        Logger $logger
    ) {
        $this->directoryList = $directoryList;
        $this->fileDriver = $fileDriver;
        $this->logger = $logger;
    }

    /**
     * Clears Scalapay log files older than 6 months.
     *
     * @return bool
     */
    public function execute(): bool
    {
        try {
            // get log files
            $logFolder = $this->directoryList->getPath(DirectoryList::LOG);
            $logFiles = $this->fileDriver->readDirectory($logFolder);

            // loop each log file
            foreach ($logFiles as $logFile) {
                // skip log if it isn' a Scalapay log
                $fileName = explode($logFolder . DIRECTORY_SEPARATOR, $logFile)[1] ?? null;
                if (!$fileName || substr($fileName, 0, 9) !== self::SCALAPAY_LOG_PREFIX) {
                    continue;
                }

                // skip log if the file name is corrupted
                $fileName = explode('_', str_replace('.log', '', $fileName));
                if (count($fileName) < 3) {
                    continue;
                }

                // get log file date
                $logFileMonth = (string) $fileName[1];
                $logFileYear = (string) $fileName[2];
                $logFileDate = $this->getLogFileDate($logFileMonth, $logFileYear);

                // skip if the log file isn't older than 6 months
                if ($logFileDate > strtotime('-180 days')) {
                    continue;
                }

                // delete the log file
                $this->fileDriver->deleteFile($logFile);
            }

            return true;
        } catch (Exception $e) {
            $this->logger->critical(__CLASS__ . ': Impossible delete the log file: ' .  $e->getMessage());
            return false;
        }
    }

    /**
     * Returns the log file date relative to the last day of the month in seconds.
     *
     * @param string $month
     * @param string $year
     * @return false|int
     */
    private function getLogFileDate(string $month, string $year)
    {
        $date = strtotime("$month/01/$year 00:00:00");
        $day = date('d', strtotime(date('Y-m-t', $date)));
        return strtotime("$month/$day/$year 00:00:00");
    }
}
