<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Helper;

use Exception;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Api\Data\CartInterface;
use Magento\Quote\Model\QuoteManagement;
use Magento\Sales\Model\ResourceModel\Order\Status\Collection as StatusCollection;
use Scalapay\Scalapay\Helper\Payment as PaymentHelper;
use Scalapay\Scalapay\Logger\Logger;
use Scalapay\Scalapay\Model\ApiPayment;
use Scalapay\Scalapay\Model\ResourceModel\OrderToken;

/**
 * Class Order
 *
 * @author Sclapay Plugin Integration Team
 * @package Scalapay\Scalapay\Helper
 */
class Order extends AbstractHelper
{
    /** @var CartRepositoryInterface $cartRepositoryInterface */
    private $cartRepositoryInterface;

    /** @var QuoteManagement $quoteManagement */
    private $quoteManagement;

    /** @var StatusCollection $statusCollection */
    private $statusCollection;

    /** @var PaymentHelper $paymentHelper */
    private $paymentHelper;

    /** @var Logger $logger */
    private $logger;

    /** @var ApiPayment $apiPayment */
    private $apiPayment;

    /** @var OrderToken $orderToken */
    private $orderToken;

    /**
     * Index constructor.
     *
     * @param Context $context
     * @param CartRepositoryInterface $cartRepositoryInterface
     * @param QuoteManagement $quoteManagement
     * @param StatusCollection $statusCollection
     * @param Payment $paymentHelper
     * @param Logger $logger
     * @param ApiPayment $apiPayment
     * @param OrderToken $orderToken
     */
    public function __construct(
        Context $context,
        CartRepositoryInterface $cartRepositoryInterface,
        QuoteManagement $quoteManagement,
        StatusCollection $statusCollection,
        PaymentHelper $paymentHelper,
        Logger $logger,
        ApiPayment $apiPayment,
        OrderToken $orderToken
    ) {
        parent::__construct($context);
        $this->cartRepositoryInterface = $cartRepositoryInterface;
        $this->quoteManagement = $quoteManagement;
        $this->statusCollection = $statusCollection;
        $this->paymentHelper = $paymentHelper;
        $this->logger = $logger;
        $this->apiPayment = $apiPayment;
        $this->orderToken = $orderToken;
    }

    /**
     * Returns the quote by the given Scalapay Order Token.
     *
     * @param string $orderToken
     * @return CartInterface
     * @throws NoSuchEntityException
     * @throws Exception
     */
    public function getQuoteByOrderToken(string $orderToken): CartInterface
    {
        // get quote id
        $quoteId = (int) $this->orderToken->getQuoteId($orderToken);
        if (!$quoteId) {
            throw new Exception('Impossible retrieve the quote id from the order token ' . $orderToken);
        }

        // get and check the quote
        $quote = $this->cartRepositoryInterface->get($quoteId);
        if (!$quote || !$quote->getId()) {
            throw new Exception('Impossible retrieve the quote from the order quote id ' . $quoteId);
        }

        if (!$quote->getIsActive()) {
            throw new Exception('Invalid quote ' . $quoteId . ': it is not active');
        }

        // return quote
        return $quote;
    }

    /**
     * Checks the payment.
     *
     * @param CartInterface $quote
     * @return bool
     * @throws Exception
     */
    public function paymentIsAuthorized(CartInterface $quote): bool
    {
        // get and check payment method
        $paymentMethod = $quote->getPayment()->getMethod();
        if (!$paymentMethod || !$this->paymentHelper->isScalapayPayment($paymentMethod)) {
            $this->logger->critical(
                __CLASS__ . ': Payment method is invalid' .
                ' - Quote Id: ' . $quote->getId() .
                ' - Quote Payment Method: ' . $paymentMethod
            );

            return false;
        }

        // get and check order token
        $orderToken = $quote->getScalapayOrderToken();
        if (!$orderToken) {
            $this->logger->critical(
                __CLASS__ . ': Scalapay order token is invalid' .
                ' - Quote Id: ' . $quote->getId() .
                ' - Quote Order Token: ' . $orderToken
            );

            return false;
        }

        // get payment status
        $paymentStatus = $this->apiPayment->get($orderToken, $paymentMethod);

        // check payment status payload
        if (!isset($paymentStatus['token']) || $orderToken !== $paymentStatus['token']) {
            $this->logger->critical(
                __CLASS__ . ': Scalapay order token mismatch' .
                ' - Quote Id: ' . $quote->getId() .
                ' - Quote Order Token: ' . $orderToken .
                ' - Payment Status Object: ' . json_encode($paymentStatus)
            );

            return false;
        }

        if (!isset($paymentStatus['totalAmount']['amount'])
            || $this->totalAmountMismatch($quote, $paymentStatus)
        ) {
            $this->logger->critical(
                __CLASS__ . ': Scalapay total amount mismatch' .
                ' - Quote Id: ' . $quote->getId() .
                ' - Quote Grand Total: ' . $quote->getGrandTotal() .
                ' - Payment Status Object: ' . json_encode($paymentStatus)
            );

            return false;
        }

        if (!isset($paymentStatus['orderDetails']['merchantReference'])
            || $quote->getReservedOrderId() != $paymentStatus['orderDetails']['merchantReference']
        ) {
            $this->logger->critical(
                __CLASS__ . ': Scalapay merchant reference mismatch' .
                ' - Quote Id: ' . $quote->getId() .
                ' - Quote Reserved Order Id: ' . $quote->getReservedOrderId() .
                ' - Payment Status Object: ' . json_encode($paymentStatus)
            );

            return false;
        }

        if (!isset($paymentStatus['status']) || $paymentStatus['status'] !== 'authorized') {
            $this->logger->critical(
                __CLASS__ . ': Scalapay payment status is not authorized.' .
                ' - Quote Id: ' . $quote->getId() .
                ' - Payment Status Object: ' . json_encode($paymentStatus)
            );

            return false;
        }

        if (!isset($paymentStatus['captureStatus']) || $paymentStatus['captureStatus'] !== 'pending') {
            $this->logger->critical(
                __CLASS__ . ': Scalapay payment capture status is not pending.' .
                ' - Quote Id: ' . $quote->getId() .
                ' - Payment Status Object: ' . json_encode($paymentStatus)
            );

            return false;
        }

        return true;
    }

    /**
     * Place an order.
     *
     * @param CartInterface $quote
     * @return int
     * @throws CouldNotSaveException
     */
    public function placeOrder(CartInterface $quote): int
    {
        // collect totals
        $quote->setTotalsCollectedFlag(false);
        $quote->collectTotals();

        // place the order
        return $this->quoteManagement->placeOrder($quote->getId(), $quote->getPayment());
    }

    /**
     * Unsets reserved order id from quote by a given Scalapay order token.
     *
     * @param string $orderToken
     * @return void
     */
    public function unsetReserveOrderIdByOrderToken(string $orderToken)
    {
        try {
            // check order token
            if (!$orderToken) {
                throw new Exception('Invalid order token.');
            }

            // get quote
            $quote = $this->getQuoteByOrderToken($orderToken);

            // unset reserved order id
            $this->unsetReservedOrderId($quote);
        } catch (Exception $e) {
            $this->logger->critical(
                __CLASS__ . ': Impossible unset the reserved order id from quote: ' . $e->getMessage()
            );
        }
    }

    /**
     * Unsets reserved order id from quote.
     *
     * @param CartInterface $quote
     * @return void
     */
    public function unsetReservedOrderId(CartInterface $quote)
    {
        $quote->setReservedOrderId(null);
        $this->cartRepositoryInterface->save($quote);
    }

    /**
     * Returns the associated state by the given status.
     *
     * @param string $orderStatus
     * @return string
     * @throws Exception
     */
    public function getStateByStatus(string $orderStatus): string
    {
        // get status collection
        $statuses = $this->statusCollection->joinStates();

        // check statuses
        if (!$statuses->count()) {
            // throw exception if the status does not have an associated state
            throw new Exception('Order statuses does not exists.');
        }

        // loop each status object
        $statuses = $statuses->getData();
        foreach ($statuses as $status) {
            // skip unassociated status
            if ($status['status'] !== $orderStatus) {
                continue;
            }

            // return state
            return $status['state'];
        }

        // throw exception if the status does not have an associated state
        throw new Exception('Impossible retrieve a valid state from ' . $orderStatus . ' status.');
    }

    /**
     * Returns true if the total amounts mismatch else false.
     *
     * @param CartInterface $quote
     * @param array $paymentStatus
     * @return bool
     */
    public function totalAmountMismatch(CartInterface $quote, array $paymentStatus): bool
    {
        // get magento and scalapay total amounts
        $magentoTotalAmount = $quote->getGrandTotal();
        $scalapayTotalAmount = $paymentStatus['totalAmount']['amount'];

        // round if the magento total amount have more than 2 decimal digits
        if ($this->hasMoreThanTwoDecimals($magentoTotalAmount)) {
            $magentoTotalAmount = round((float) $magentoTotalAmount, 2);
            $scalapayTotalAmount = round((float) $scalapayTotalAmount, 2);
        }

        // return true if mismatch else false
        return $magentoTotalAmount != $scalapayTotalAmount;
    }

    /**
     * Returns if decimal digits are over than two without considering zeros.
     *
     * @param $number
     * @return bool
     */
    public function hasMoreThanTwoDecimals($number): bool
    {
        $decimalPart = explode('.', (string) $number)[1] ?? '';
        return strlen($decimalPart) > 2 && preg_match('/[1-9]/', substr($decimalPart, 2));
    }
}
