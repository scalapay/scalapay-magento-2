<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Helper;

use Exception;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;

/**
 * Class Product
 *
 * @author Sclapay Plugin Integration Team
 * @package Scalapay\Scalapay\Helper
 */
class Product extends AbstractHelper
{
    /** @var ProductRepositoryInterface $productRepositoryInterface */
    private $productRepositoryInterface;

    /**
     * Product constructor.
     *
     * @param Context $context
     * @param ProductRepositoryInterface $productRepositoryInterface
     */
    public function __construct(
        Context $context,
        ProductRepositoryInterface $productRepositoryInterface
    ) {
        parent::__construct($context);
        $this->productRepositoryInterface = $productRepositoryInterface;
    }

    /**
     * Returns the product type by a given product id.
     *
     * @param $productId
     * @return string
     */
    public function getProductType($productId): string
    {
        try {
            return $this->productRepositoryInterface->getById($productId)->getTypeId();
        } catch (Exception $e) {
            return '';
        }
    }
}
