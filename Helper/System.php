<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\ProductMetadataInterface;

/**
 * Class Payment
 *
 * @author Sclapay Plugin Integration Team
 * @package Scalapay\Scalapay\Helper
 */
class System extends AbstractHelper
{
    /** @var string COMMUNITY_EDITION */
    const COMMUNITY_EDITION = 'community';

    /** @var ProductMetadataInterface $productMetadataInterface */
    private $productMetadataInterface;

    /**
     * System constructor.
     *
     * @param Context $context
     * @param ProductMetadataInterface $productMetadataInterface
     */
    public function __construct(
        Context $context,
        ProductMetadataInterface $productMetadataInterface
    ) {
        parent::__construct($context);
        $this->productMetadataInterface = $productMetadataInterface;
    }

    /**
     * Returns true if Magento is a community edition else false.
     *
     * @return bool
     */
    public function isCommunityEdition(): bool
    {
        $magentoEdition = strtolower($this->productMetadataInterface->getEdition());
        return strpos($magentoEdition, self::COMMUNITY_EDITION) !== false;
    }

    /**
     * Returns true if the Magento version is lower than the given version for comparison.
     *
     * @param string $version
     * @return bool
     */
    public function magentoVersionIsLowerThan(string $version): bool
    {
        $magentoVersion = $this->productMetadataInterface->getVersion();
        return version_compare($magentoVersion, $version, '<');
    }
}
