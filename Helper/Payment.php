<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Scalapay\Scalapay\Gateway\Settings\Scalapay\Settings as ScalapaySettings;

/**
 * Class Payment
 *
 * @author Sclapay Plugin Integration Team
 * @package Scalapay\Scalapay\Helper
 */
class Payment extends AbstractHelper
{
    /**
     * Returns true if the given payment code is a Scalapay payment code else false.
     *
     * @param string $paymentCode
     * @return bool
     */
    public function isScalapayPayment(string $paymentCode): bool
    {
        return $paymentCode === ScalapaySettings::CODE;
    }
}
