<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Helper;

use Exception;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Scalapay\Scalapay\Gateway\Settings\Scalapay\Settings as ScalapaySettings;
use Scalapay\Scalapay\Helper\Product as ProductHelper;

/**
 * Class Restriction
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Helper
 */
class Restriction extends AbstractHelper
{
    /** @var ResourceConnection $resourceConnection */
    private $resourceConnection;

    /** @var ScalapaySettings $scalapaySettings */
    private $scalapaySettings;

    /** @var ProductHelper $productHelper */
    private $productHelper;

    /** @var string[] ALLOWED_COUNTRIES */
    public const ALLOWED_COUNTRIES = [
        ['label' => 'Belgique (Belgium)', 'value' => 'BE'], ['label' => 'Deutschland (Germany)', 'value' => 'DE'],
        ['label' => 'España (Spain)', 'value' => 'ES'], ['label' => 'Ελλάδα (Greece)', 'value' => 'GR'],
        ['label' => 'France (France)', 'value' => 'FR'], ['label' => 'Guyane (French Guiana)', 'value' => 'GF'],
        ['label' => 'Guadeloupe (Guadeloupe)', 'value' => 'GP'], ['label' => 'Italia (Italy)', 'value' => 'IT'],
        ['label' => 'La Réunion (Réunion)', 'value' => 'RE'], ['label' => 'Martinique (Martinique)', 'value' => 'MQ'],
        ['label' => 'Mayotte (Mayotte)', 'value' => 'YT'], ['label' => 'Nederland (Netherlands)', 'value' => 'NL'],
        ['label' => 'Österreich (Austria)', 'value' => 'AT'], ['label' => 'Portugal (Portugal)', 'value' => 'PT'],
        ['label' => 'Slovenija (Slovenia)', 'value' => 'SI'], ['label' => 'Suomi (Finland)', 'value' => 'FI']
    ];

    /** @var string[] ALLOWED_LANGUAGES */
    public const ALLOWED_LANGUAGES = [
        ['label' => 'Deutsch (German)', 'value' => 'de'],
        ['label' => 'English (English)', 'value' => 'en'],
        ['label' => 'Español (Spanish)', 'value' => 'es'],
        ['label' => 'Français (French)', 'value' => 'fr'],
        ['label' => 'Italiano (Italian)', 'value' => 'it'],
        ['label' => 'Português (Portuguese)', 'value' => 'pt']
    ];

    /**
     * Restriction constructor.
     *
     * @param Context $context
     * @param ResourceConnection $resourceConnection
     * @param ScalapaySettings $scalapaySettings
     * @param Product $productHelper
     */
    public function __construct(
        Context $context,
        ResourceConnection $resourceConnection,
        ScalapaySettings $scalapaySettings,
        ProductHelper $productHelper
    ) {
        parent::__construct($context);
        $this->resourceConnection = $resourceConnection;
        $this->scalapaySettings = $scalapaySettings;
        $this->productHelper = $productHelper;
    }

    /**
     * Returns true if a set of products is in a disabled category list else false.
     *
     * @param int[] $productIds
     * @return bool
     * @throws Exception
     */
    public function isProductListInDisabledCategories(array $productIds): bool
    {
        if (empty($productIds)) {
            return false;
        }

        $connection = $this->resourceConnection->getConnection();
        $catalogCategoryProductTableName = $this->resourceConnection->getTableName('catalog_category_product');
        $catalogCategoryEntityTableName = $this->resourceConnection->getTableName('catalog_category_entity');

        $qry = $connection
            ->select()
            ->distinct()
            ->from($catalogCategoryProductTableName, 'category_id')
            ->where('product_id IN (?)', $productIds);
        $leafCategoriesIds = $connection->fetchCol($qry);

        $qry = $connection
            ->select()
            ->from($catalogCategoryEntityTableName, 'path')
            ->where('entity_id IN (?)', $leafCategoriesIds);

        $categoriesPaths = $connection->fetchCol($qry);
        $productsCategories = array_unique(array_reduce($categoriesPaths, static function ($acc, $categoryPath) {
            return array_merge($acc, explode('/', $categoryPath));
        }, []));

        $disabledCategories = $this->scalapaySettings->getDisabledCategories();

        return !empty(array_intersect($disabledCategories, $productsCategories));
    }

    /**
     * Returns true if there are products with a disabled product type else false.
     *
     * @param int[] $productIds
     * @param string $scalapayType
     * @return bool
     * @throws Exception
     */
    public function isProductListInDisabledProductTypes(array $productIds): bool
    {
        // init vars
        $productsTypes = [];

        // check product ids array
        if (empty($productIds)) {
            return false;
        }

        // check disabled product types array
        $disabledProductTypes = $this->scalapaySettings->getDisabledProductTypes();
        if (empty($disabledProductTypes)) {
            return false;
        }

        // push product types into the specific array
        foreach ($productIds as $productId) {
            $productType = $this->productHelper->getProductType($productId);
            if ($productType && !in_array($productType, $productsTypes)) {
                $productsTypes[] = $productType;
            }
        }

        // return if there are disabled product types
        return !empty(array_intersect($disabledProductTypes, $productsTypes));
    }

    /**
     * Returns false if the given country is not in the disabled country list else true.
     *
     * @param string|null $countryCode
     * @return bool
     */
    public function isCountryDisabled(?string $countryCode): bool
    {
        // avoid checks if the country code is not available
        if (!$countryCode) {
            return false;
        }

        // disable countries not in allowed countries array
        if (!in_array(
            $countryCode,
            array_column(self::ALLOWED_COUNTRIES, 'value')
        )
        ) {
            return true;
        }

        // check if the country has been disabled
        $disabledCountries = $this->scalapaySettings->getDisabledCountries();
        return in_array($countryCode, $disabledCountries);
    }

    /**
     * Returns false if the given language is not in the disabled language list else true.
     *
     * @param string $languageCode
     * @return bool
     */
    public function isLanguageDisabled(string $languageCode): bool
    {
        // avoid checks if the language code is not available
        if (!$languageCode) {
            return false;
        }

        // disable languages not in allowed languages array
        if (!in_array(
            explode('_', $languageCode)[0],
            array_column(self::ALLOWED_LANGUAGES, 'value')
        )
        ) {
            return true;
        }

        // check if the language has been disabled
        $disabledLanguages = $this->scalapaySettings->getDisabledLanguages();
        return in_array($languageCode, $disabledLanguages);
    }

    /**
     * Returns false if the given currency is allowed else true.
     * EUR is the only currency allowed at the moment.
     *
     * @param string $currencyCode
     * @return bool
     */
    public function isCurrencyDisabled(string $currencyCode): bool
    {
        return $currencyCode !== 'EUR';
    }
}
