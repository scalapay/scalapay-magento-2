<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;

/**
 * Class Config
 *
 * @author Sclapay Plugin Integration Team
 * @package Scalapay\Scalapay\Helper
 */
class Config extends AbstractHelper
{
    /**
     * Retrieve a configuration by a given configuration key.
     *
     * @param string $configKey
     * @param string $scope
     * @return string
     */
    public function getConfig(string $configKey, string $scope = ScopeInterface::SCOPE_STORES): string
    {
        return (string) $this->scopeConfig->getValue($configKey, $scope);
    }

    /**
     * Retrieve a payment configuration by a given field.
     *
     * @param string $paymentType
     * @param string $field
     * @return string
     */
    public function getPaymentConfig(string $paymentType, string $field): string
    {
        return (string) $this->getConfig('payment/' . $paymentType . '/'. $field);
    }

    /**
     * Retrieve a tax configuration by a given field.
     *
     * @param $field
     * @return int
     */
    public function getTaxConfig($field): int
    {
        return (int) $this->getConfig('tax/' . $field);
    }

    /**
     * Returns true if a payment is enabled else false.
     *
     * @param string $paymentType
     * @return bool
     */
    public function isPaymentEnabled(string $paymentType): bool
    {
        return ((int) $this->getPaymentConfig($paymentType, 'active')) === 1;
    }
}
