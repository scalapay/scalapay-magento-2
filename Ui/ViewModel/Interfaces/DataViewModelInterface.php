<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Ui\ViewModel\Interfaces;

use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Interface DataViewModelInterface
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Ui\ViewModel
 */
interface DataViewModelInterface
{
    /**
     * Returns the price.
     *
     * @return float
     * @throws NoSuchEntityException
     */
    public function getPrice(): float;

    /**
     * Returns the product id into an array.
     *
     * @return array
     */
    public function getProductsIds(): array;

    /**
     * Returns the country.
     *
     * @return string
     */
    public function getCountry(): string;
}
