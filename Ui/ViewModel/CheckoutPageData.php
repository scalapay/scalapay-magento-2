<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Ui\ViewModel;

/**
 * Class CheckoutPageData
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Ui\ViewModel
 */
class CheckoutPageData extends CartPageData
{
}
