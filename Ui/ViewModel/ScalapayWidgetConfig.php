<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Ui\ViewModel;

use Exception;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Locale\Resolver as LocaleResolver;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Store\Model\StoreManagerInterface;
use Scalapay\Scalapay\Gateway\Settings\Scalapay\Settings as ScalapaySettings;
use Scalapay\Scalapay\Helper\Config as ConfigHelper;
use Scalapay\Scalapay\Helper\Restriction as RestrictionHelper;
use Scalapay\Scalapay\Helper\System as SystemHelper;

/**
 * Class ScalapayWidgetConfig
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Ui\ViewModel
 */
class ScalapayWidgetConfig implements ArgumentInterface
{
    /** @var LocaleResolver $localeResolver */
    private $localeResolver;

    /** @var ScalapaySettings $scalapaySettings */
    private $scalapaySettings;

    /** @var ConfigHelper $configHelper */
    private $configHelper;

    /** @var RestrictionHelper $restrictionHelper */
    private $restrictionHelper;

    /** @var StoreManagerInterface $storeManagerInterface */
    private $storeManagerInterface;

    /** @var string $sectionType */
    private $sectionType;

    /** @var SerializerInterface $serializerInterface */
    private $serializerInterface;

    /** @var SystemHelper $systemHelper */
    private $systemHelper;

    /**
     * ScalapayWidgetConfig constructor.
     *
     * @param LocaleResolver $localeResolver
     * @param ScalapaySettings $scalapaySettings
     * @param ConfigHelper $configHelper
     * @param RestrictionHelper $restrictionHelper
     * @param StoreManagerInterface $storeManagerInterface
     * @param SerializerInterface $serializerInterface
     * @param SystemHelper $systemHelper
     * @param string $sectionType
     */
    public function __construct(
        LocaleResolver $localeResolver,
        ScalapaySettings $scalapaySettings,
        ConfigHelper $configHelper,
        RestrictionHelper $restrictionHelper,
        StoreManagerInterface $storeManagerInterface,
        SerializerInterface $serializerInterface,
        SystemHelper $systemHelper,
        string $sectionType
    ) {
        $this->localeResolver = $localeResolver;
        $this->scalapaySettings = $scalapaySettings;
        $this->configHelper = $configHelper;
        $this->restrictionHelper = $restrictionHelper;
        $this->storeManagerInterface = $storeManagerInterface;
        $this->serializerInterface = $serializerInterface;
        $this->systemHelper = $systemHelper;
        $this->sectionType = $sectionType;
    }

    /**
     * Returns a configuration by a given path.
     *
     * @param string $path
     * @param string|null $section
     * @return string
     */
    private function getConfig(string $path, ?string $section = null): string
    {
        return $this->configHelper->getConfig(
            'scalapay/widget/' . ($section ?? $this->sectionType) . '/' . $path
        );
    }

    /**
     * Returns true if the widget can be displayed else false.
     *
     * @param array $productsIds
     * @param string $currentCountry
     * @return bool
     * @throws Exception
     * @throws NoSuchEntityException
     */
    public function canDisplay(
        array $productsIds = [],
        string $currentCountry = ''
    ): bool {
        if (!$this->scalapaySettings->isActive()) {
            return false;
        }

        if ($this->restrictionHelper->isProductListInDisabledCategories($productsIds)) {
            return false;
        }

        if ($this->restrictionHelper->isProductListInDisabledProductTypes($productsIds)) {
            return false;
        }

        if ($this->restrictionHelper->isCountryDisabled($currentCountry)) {
            return false;
        }

        $currentLanguage = $this->localeResolver->getLocale();
        if ($this->restrictionHelper->isLanguageDisabled($currentLanguage)) {
            return false;
        }

        $currentCurrency = $this->storeManagerInterface->getStore()->getCurrentCurrency()->getCode();
        if ($this->restrictionHelper->isCurrencyDisabled($currentCurrency)) {
            return false;
        }

        return true;
    }

    /**
     * Returns true if the widget is hidden else false.
     *
     * @return bool
     */
    public function isHidden(): bool
    {
        return (bool) $this->getConfig('hide_widget');
    }

    /**
     * Returns the min amount.
     *
     * @return float
     * @throws Exception
     */
    public function getMinAmount(): float
    {
        return $this->scalapaySettings->getMinAmount();
    }

    /**
     * Returns the max amount.
     *
     * @return float
     * @throws Exception
     */
    public function getMaxAmount(): float
    {
        return $this->scalapaySettings->getMaxAmount();
    }

    /**
     * Returns the currency position.
     *
     * @return string
     */
    public function getCurrencyPosition(): string
    {
        return $this->getConfig('currency_position', 'currency');
    }

    /**
     * Returns the currency display.
     *
     * @return string
     */
    public function getCurrencyDisplay(): string
    {
        return $this->getConfig('currency_display', 'currency');
    }

    /**
     * Returns the amount separator.
     *
     * @return string
     */
    public function getAmountSeparator(): string
    {
        return $this->getConfig('amount_separator', 'currency');
    }

    /**
     * Returns the amount selectors.
     *
     * @return string
     */
    public function getAmountSelectors(): string
    {
        $selectorsArray = preg_split('/[\n\r]+/', $this->getConfig('amount_selector_array'));
        return $this->serializerInterface->serialize($selectorsArray);
    }

    /**
     * Returns the mode formatted in string.
     *
     * @return string
     */
    public function getMode(): string
    {
        return $this->scalapaySettings->getLiveMode() ? 'production' : 'test';
    }

    /**
     * Returns the text position.
     *
     * @return string
     */
    public function getTextPosition(): string
    {
        return $this->getConfig('text_position');
    }

    /**
     * Returns the current language.
     *
     * @return string
     */
    public function getCurrentLanguage(): string
    {
        $locale = $this->localeResolver->getLocale();
        return substr($locale, 0, 2);
    }

    /**
     * Returns the section type.
     *
     * @return string
     */
    public function getSectionType(): string
    {
        return $this->sectionType;
    }

    /**
     * Returns the merchant token.
     *
     * @return string
     */
    public function getMerchantToken(): string
    {
        return $this->scalapaySettings->getLiveMode() ?
            $this->scalapaySettings->getProductionMerchantToken() :
            $this->scalapaySettings->getTestMerchantToken();
    }

    /**
     * Returns true if the magento version is lower than 2.4.0 else false.
     *
     * @return bool
     */
    public function magentoVersionIsLowerThan240(): bool
    {
        return $this->systemHelper->magentoVersionIsLowerThan('2.4.0');
    }
}
