<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Ui\ViewModel;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Helper\Data as CatalogHelper;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Scalapay\Scalapay\Helper\Config as ConfigHelper;
use Scalapay\Scalapay\Ui\ViewModel\Interfaces\DataViewModelInterface;

/**
 * Class ProductPageData
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Ui\ViewModel
 */
class ProductPageData implements ArgumentInterface, DataViewModelInterface
{
    /** @var RequestInterface $request */
    private $request;

    /** @var ProductRepositoryInterface $productRepository */
    private $productRepository;

    /** @var CatalogHelper $catalogHelper */
    private $catalogHelper;

    /** @var ConfigHelper $configHelper */
    private $configHelper;

    /**
     * ProductPageData constructor.
     *
     * @param RequestInterface $request
     * @param ProductRepositoryInterface $productRepository
     * @param CatalogHelper $catalogHelper
     * @param ConfigHelper $configHelper
     */
    public function __construct(
        RequestInterface $request,
        ProductRepositoryInterface $productRepository,
        CatalogHelper $catalogHelper,
        ConfigHelper $configHelper
    ) {
        $this->request = $request;
        $this->productRepository = $productRepository;
        $this->catalogHelper = $catalogHelper;
        $this->configHelper = $configHelper;
    }

    /**
     * Returns the product id.
     *
     * @return int
     */
    private function getProductId(): int
    {
        if ($this->request->getParam('product_id')) {
            return (int) $this->request->getParam('product_id');
        } else {
            return (int) $this->request->getParam('id');
        }
    }

    /**
     * Returns the product.
     *
     * @return ProductInterface
     * @throws NoSuchEntityException
     */
    private function getProduct(): ProductInterface
    {
        return $this->productRepository->getById($this->getProductId());
    }

    /**
     * Returns the price.
     *
     * @return float
     * @throws NoSuchEntityException
     */
    public function getPrice(): float
    {
        // get the product
        $product = $this->getProduct();

        // return price without including tax if they are not visible on the frontend
        if ($this->configHelper->getTaxConfig('display/type') === 1) {
            return (float) $product->getFinalPrice();
        }

        // return price including tax
        return (float) $this->catalogHelper->getTaxPrice(
            $product,
            $product->getFinalPrice(),
            true
        );
    }

    /**
     * Returns the product id into an array.
     *
     * @return array
     */
    public function getProductsIds(): array
    {
        return [$this->getProductId()];
    }

    /**
     * Returns the country.
     *
     * @return string
     */
    public function getCountry(): string
    {
        return '';
    }
}
