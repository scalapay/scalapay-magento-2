<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Ui\ViewModel;

use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Quote\Model\Quote;
use Scalapay\Scalapay\Ui\ViewModel\Interfaces\DataViewModelInterface;

/**
 * Class CartPageData
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Ui\ViewModel
 */
class CartPageData implements ArgumentInterface, DataViewModelInterface
{
    /** @var CheckoutSession $checkoutSession */
    private $checkoutSession;

    /**
     * CartPageData constructor.
     *
     * @param CheckoutSession $checkoutSession
     */
    public function __construct(CheckoutSession $checkoutSession)
    {
        $this->checkoutSession = $checkoutSession;
    }

    /**
     * Returns the quote.
     *
     * @return Quote
     */
    private function getQuote(): Quote
    {
        return $this->checkoutSession->getQuote();
    }

    /**
     * Returns the quote grand total.
     *
     * @return float
     */
    public function getPrice(): float
    {
        return (float) $this->getQuote()->getGrandTotal();
    }

    /**
     * Returns quote product ids.
     *
     * @return array
     */
    public function getProductsIds(): array
    {
        $items = $this->getQuote()->getItems();
        if (empty($items)) {
            return [];
        }

        return array_map(static function ($item) {
            return $item->getProductId();
        }, $items);
    }

    /**
     * Returns billing address country.
     *
     * @return string
     */
    public function getCountry(): string
    {
        if (!$this->getQuote()->getBillingAddress()) {
            return '';
        }

        return (string) $this->getQuote()->getBillingAddress()->getCountryId();
    }
}
