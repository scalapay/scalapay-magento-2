<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Setup;

use Exception;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\App\ResourceConnection;
use Scalapay\Scalapay\Helper\Config as ConfigHelper;
use Scalapay\Scalapay\Helper\Restriction as RestrictionHelper;
use Scalapay\Scalapay\Logger\Logger;

/**
 * Class UpgradeData
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Setup
 */
class UpgradeData implements UpgradeDataInterface
{
    /** @var ResourceConnection $resourceConnection */
    private $resourceConnection;

    /** @var ConfigHelper $configHelper */
    private $configHelper;

    /** @var Logger $logger */
    private $logger;

    /** @var string[] CONVERSION_CASES */
    const CONVERSION_CASES = ['specific_countries', 'specific_languages', 'show_widget'];

    /** @var string[] SKIP_BY_UPPER_LEVEL_CASES */
    const SKIP_BY_UPPER_LEVEL_CASES = ['currency_position', 'currency_display'];

    /**
     * UpgradeData constructor.
     *
     * @param ResourceConnection $resourceConnection
     * @param ConfigHelper $configHelper
     * @param Logger $logger
     */
    public function __construct(
        ResourceConnection $resourceConnection,
        ConfigHelper $configHelper,
        Logger $logger
    ) {
        $this->resourceConnection = $resourceConnection;
        $this->configHelper = $configHelper;
        $this->logger = $logger;
    }

    /**
     * Upgrade data entrypoint.
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     * @throws Exception
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        // init setup
        $setup->startSetup();

        // run upgrades
        try {
            $this->v400($context);
        } catch (Exception $e) {
            // intercept the exception and log it
            $this->logger->critical(
                __CLASS__ . ': Scalapay_Scalapay - Data upgrade failed.' .
                ' - Exception: ' . $e->getMessage()
            );

            // throw it again
            throw new Exception($e->getMessage());
        }

        // finish setup upgrade
        $setup->endSetup();
    }

    /**
     * Runs upgrade data for plugin versions older than v4.0.0.
     *
     * @param ModuleContextInterface $context
     * @return void
     */
    private function v400(ModuleContextInterface $context): void
    {
        // setup upgrade for version 4.0.0
        if ($context->getVersion() && version_compare($context->getVersion(), '4.0.0', '<')) {
            // start v4.0.0 data upgrade
            $this->logger->info(__CLASS__ . ': Start data migration for version 4.0.0');

            // get core config data table name
            $coreConfigDataTable = $this->resourceConnection->getTableName('core_config_data');

            // loop config sections
            $sections = ['payment', 'widget'];
            foreach ($sections as $section) {
                // get and check Scalapay type
                $scalapayType = $this->getScalapayType($section);
                if (!$scalapayType) {
                    $this->logger->error(
                        __CLASS__ . ': Skip data migration: No scalapay product enabled. Section: ' . $section
                    );

                    continue;
                }

                // get and check old path
                $oldPath = $this->getOldConfigPrefixPath($section, $scalapayType);
                if (!$oldPath) {
                    $this->logger->error(
                        __CLASS__ . ': Skip data migration: Impossible retrieve old path. ' .
                        'Section: ' . $section . ' - Scalapay type: ' . $scalapayType
                    );

                    continue;
                }

                // get configs
                $select = $this->resourceConnection->getConnection()->select()
                    ->from($coreConfigDataTable)
                    ->where('path LIKE ?', $oldPath . '/%');
                $configs = $this->resourceConnection->getConnection()->fetchAll($select);

                // check configs
                if (!count($configs)) {
                    $this->logger->error(
                        __CLASS__ . ': Skip data migration: No Scalapay config found. ' .
                        'Section: ' . $section . ' - Scalapay type: ' . $scalapayType
                    );

                    continue;
                }

                // migrate the configs
                foreach ($configs as $config) {
                    try {
                        $this->migrate($coreConfigDataTable, $config, $section);
                    } catch (Exception $e) {
                        // log and skip
                        $this->logger->error(
                            __CLASS__ . ': Impossible to migrate the configuration ' . json_encode($config) .
                            ' - Exception: ' . $e->getMessage()
                        );
                        continue;
                    }
                }
            }

            // disable deprecated Scalapay payment methods
            foreach (['payin4', 'scalapay_product_suite', 'paylater'] as $deprecatedPaymentMethod) {
                $this->resourceConnection->getConnection()->update(
                    $coreConfigDataTable,
                    ['value' => 0],
                    ['path = ?' => 'payment/' . $deprecatedPaymentMethod . '/active']
                );
            }

            // finish v4.0.0 data upgrade
            $this->logger->info(__CLASS__ . ': Finish data migration for version 4.0.0');
        }
    }

    /**
     * Returns the Scalapay type by the section.
     *
     * @param string $section
     * @return false|string
     */
    private function getScalapayType(string $section)
    {
        // get payment types based on config section
        $paymentTypes = ['scalapay_product_suite', 'scalapay', 'payin4'];

        // return the first enabled Scalapay payment considering the array priority
        foreach ($paymentTypes as $paymentType) {
            if ($this->configHelper->isPaymentEnabled($paymentType)) {
                if ($paymentType === 'scalapay' && $section === 'widget') {
                    return 'payin3';
                }

                return $paymentType;
            }
        }

        // return false if no one Scalapay payment is enabled
        return false;
    }

    /**
     * Migrates the given configuration.
     *
     * @param string $coreConfigDataTable
     * @param array $config
     * @param string $section
     * @return void
     */
    private function migrate(
        string $coreConfigDataTable,
        array  $config,
        string $section
    ): void {
        // destructure config data
        $path = $config['path'] ?? null;
        $scope = $config['scope'] ?? null;
        $scopeId = $config['scope_id'] ?? null;
        $value = $config['value'] ?? null;

        // check config data
        if (!$path || !$scope || $scopeId === null) {
            $this->logger->error(
                __CLASS__ . ': Skip data migration: Missing config data. ' .
                'Scope: ' . $scope . ' - Scope Id: ' . $scopeId . ' - Path: ' . $path . ' - Value: ' . $value
            );

            return;
        }

        // get and check if the config has to be migrated
        $configId = basename($path);
        $isToBeMigrated = $this->isToBeMigrated($section, $configId);
        if (!$isToBeMigrated) {
            return;
        }

        // get and check config prefix path
        $newConfigPrefixPath = $this->getNewConfigPrefixPath($section);
        if (!$newConfigPrefixPath) {
            $this->logger->error(
                __CLASS__ . ': Skip data migration: Impossible retrieve the new config prefix path. ' .
                'Section: ' . $section . ' - Config Id: ' . $configId .
                'Scope: ' . $scope . ' - Scope Id: ' . $scopeId . ' - Path: ' . $path . ' - Value: ' . $value
            );

            return;
        }

        // retrieve the config category
        $arrayPath = explode('/', $path);
        $upperLevel = count($arrayPath) >= 2 ? $arrayPath[count($arrayPath) - 2] : null;
        $configCategory = $this->getConfigCategory($section, $configId, $upperLevel);
        if (!is_string($configCategory)) {
            $this->logger->error(
                __CLASS__ . ': Skip data migration: Impossible retrieve the config category. ' .
                'Section: ' . $section . ' - Config Id: ' . $configId . ' - ' .
                'Scope: ' . $scope . ' - Scope Id: ' . $scopeId . ' - Path: ' . $path . ' - Value: ' . $value
            );

            return;
        }

        // manage exceptional cases
        if (in_array($configId, self::SKIP_BY_UPPER_LEVEL_CASES)) {
            if ($upperLevel !== 'product') {
                return;
            }
        }

        if (in_array($configId, self::CONVERSION_CASES)) {
            // get and check converted values
            $converted = $this->convertConfiguration($config);
            if (!count($converted)) {
                $this->logger->error(
                    __CLASS__ . ': Skip data migration: Impossible to convert the configuration. ' .
                    'Section: ' . $section . ' - Config Id: ' . $configId .
                    'Scope: ' . $scope . ' - Scope Id: ' . $scopeId . ' - Path: ' . $path . ' - Value: ' . $value
                );

                return;
            }

            // update with the converted values
            $configId = $converted['config_id'];
            $value = $converted['value'];
        }

        // build new path
        $newPath = $newConfigPrefixPath . '/' . ($configCategory ? $configCategory . '/' : '') . $configId;

        // insert or update
        $this->resourceConnection->getConnection()->insertOnDuplicate(
            $coreConfigDataTable,
            [
                'scope'    => $scope,
                'scope_id' => $scopeId,
                'path'     => $newPath,
                'value'    => $value
            ],
            ['value']
        );
    }

    /**
     * Checks if a configuration is to be migrated.
     *
     * @param string $section
     * @param string $configId
     * @return bool
     */
    private function isToBeMigrated(string $section, string $configId): bool
    {
        $acceptedPaymentConfigs = [
            'active', 'sort_order', 'mode', 'test_merchant_token',
            'test_api_key', 'production_merchant_token', 'production_api_key', 'delayed_capture',
            'min_amount', 'max_amount', 'disabled_categories', 'disabled_product_types',
            'specific_countries', 'specific_languages', 'authorize_order_status', 'capture_order_status',
            'invalid_order_status'
        ];

        $acceptedWidgetConfigs = [
            'show_widget', 'amount_selector_array', 'text_position',
            'currency_position', 'currency_display'
        ];

        switch ($section) {
            case 'payment':
                return in_array($configId, $acceptedPaymentConfigs);

            case 'widget':
                return in_array($configId, $acceptedWidgetConfigs);

            default:
                return false;
        }
    }

    /**
     * Returns the old config prefix path by the given section.
     *
     * @param string $section
     * @param string $scalapayType
     * @return false|string
     */
    private function getOldConfigPrefixPath(string $section, string $scalapayType)
    {
        switch ($section) {
            case 'payment':
                return 'payment/' . $scalapayType;

            case 'widget':
                return 'scalapay/' . $scalapayType;

            default:
                return false;
        }
    }

    /**
     * Returns the new config prefix path by the given section.
     *
     * @param string $section
     * @return false|string
     */
    private function getNewConfigPrefixPath(string $section)
    {
        switch ($section) {
            case 'payment':
                return 'payment/scalapay';

            case 'widget':
                return 'scalapay/widget';

            default:
                return false;
        }
    }

    /**
     * Returns the config category by the given config id for the given section.
     *
     * @param string $section
     * @param string $configId
     * @param string|null $upperLevel
     * @return string|false
     */
    private function getConfigCategory(string $section, string $configId, ?string $upperLevel)
    {
        switch ($section) {
            case 'payment':
                return $this->getPaymentConfigCategory($configId);

            case 'widget':
                return $this->getWidgetConfigCategory($configId, $upperLevel);

            default:
                return false;
        }
    }

    /**
     * Returns the payment config categories.
     *
     * @param string $configId
     * @return string|false
     */
    private function getPaymentConfigCategory(string $configId)
    {
        // manage categories and return
        $mapping = [
            'active' => '',
            'sort_order' => '',
            'mode' => 'general',
            'test_merchant_token' => 'general',
            'test_api_key' => 'general',
            'production_merchant_token' => 'general',
            'production_api_key' => 'general',
            'delayed_capture' => 'general',
            'min_amount' => 'payment_configurations',
            'max_amount' => 'payment_configurations',
            'disabled_categories' => 'payment_configurations',
            'disabled_product_types' => 'payment_configurations',
            'specific_countries' => 'payment_configurations',
            'specific_languages' => 'payment_configurations',
            'authorize_order_status' => 'order_statuses',
            'capture_order_status' => 'order_statuses',
            'invalid_order_status' => 'order_statuses'
        ];

        return isset($mapping[$configId]) ? $mapping[$configId] : false;
    }

    /**
     * Returns the widget config category.
     *
     * @param string $configId
     * @param string|null $upperLevel
     * @return string|false
     */
    private function getWidgetConfigCategory(string $configId, ?string $upperLevel)
    {
        // manage product/cart/checkout categories
        if (in_array($configId, ['amount_selector_array', 'text_position', 'show_widget'])) {
            return in_array($upperLevel, ['product', 'cart', 'checkout']) ? $upperLevel : false;
        }

        // manage other categories and return
        $mapping = [
            'currency_position' => 'currency',
            'currency_display' => 'currency'
        ];

        return $mapping[$configId] ?? false;
    }

    /**
     * Returns the new configuration id and the new value for a given configuration.
     *
     * @param array $config
     * @return array|string[]
     */
    private function convertConfiguration(array $config): array
    {
        // retrieve the config id
        $configId = basename($config['path']);

        // manage the configurations
        switch ($configId) {
            case 'specific_countries':
            case 'specific_languages':
                return $this->convertCountriesAndLanguagesFields($config);

            case 'show_widget':
                return $this->convertShowWidgetField($config);

            default:
                return [];
        }
    }

    /**
     * Converts 'specific_countries' field in 'disabled_countries' field if 'allow_specific_countries' field is true.
     * Converts 'specific_languages' field in 'disabled_languages' field if 'allow_specific_languages' field is true.
     *
     * @param array $config
     * @return array|string[]
     */
    private function convertCountriesAndLanguagesFields(array $config)
    {
        // retrieve the config id
        $configId = basename($config['path']);

        // define the entity
        if (strpos($configId, 'countries') !== false) {
            $entity = 'countries';
        } elseif (strpos($configId, 'languages') !== false) {
            $entity = 'languages';
        } else {
            return [];
        }

        // extract the config relative path
        $relativePath = dirname($config['path']);

        // get the custom fields allow_specific_countries or allow_specific_languages
        $isConfigEnabled = (bool) $this->configHelper->getConfig(
            $relativePath . '/allow_' . $configId
        );

        // if the config is not enabled do not convert any value
        if (!$isConfigEnabled) {
            return [
                'config_id' => 'disabled_' . $entity,
                'value' => ''
            ];
        }

        // extract the old allowed values
        $oldAllowedValuesRaw = explode(
            ',',
            (string) $this->configHelper->getConfig($relativePath . '/' . $configId)
        );

        // if there are not old allowed values do not convert any value
        if (!count($oldAllowedValuesRaw)) {
            return [
                'config_id' => 'disabled_' . $entity,
                'value' => ''
            ];
        }

        // normalize the old allowed values
        $oldAllowedValues = $entity === 'countries' ?
            array_map('strtolower', $oldAllowedValuesRaw) :
            array_map(function ($value) {
                return strtolower(explode('_', $value)[0]);
            }, $oldAllowedValuesRaw);

        // filter out values not permitted
        $permittedByScalapay = $entity === 'countries' ?
            array_column(RestrictionHelper::ALLOWED_COUNTRIES, 'value') :
            array_column(RestrictionHelper::ALLOWED_LANGUAGES, 'value');

        foreach ($oldAllowedValues as $key => $allowedValue) {
            if (!in_array(strtolower($allowedValue), $permittedByScalapay)) {
                unset($oldAllowedValues[$key]);
            }
        }

        if ($entity === 'countries') {
            $oldAllowedValues = array_map('strtoupper', $oldAllowedValuesRaw);
        }

        $oldAllowedValues = array_values(array_unique($oldAllowedValues));

        // return the disabled values
        return [
            'config_id' => 'disabled_' . $entity,
            'value' => implode(',', array_values(array_diff($permittedByScalapay, $oldAllowedValues)))
        ];
    }

    /**
     * Converts 'show_widget' field in 'hide_widget' field.
     *
     * @param array $config
     * @return string[]
     */
    private function convertShowWidgetField(array $config): array
    {
        $showWidget = $config['value'] ?? null;
        if (!is_string($showWidget) && !is_int($showWidget)) {
            return [
                'config_id' => 'hide_widget',
                'value' => '0'
            ];
        }

        return [
            'config_id' => 'hide_widget',
            'value' => ((string) $showWidget === '1') ? '0' : '1'
        ];
    }
}
