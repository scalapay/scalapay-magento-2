<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Setup;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

/**
 * Class UpgradeSchema
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Setup
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /** @var ResourceConnection $resourceConnection */
    private $resourceConnection;

    /** @var SchemaSetupInterface $installer */
    private $installer;

    /**
     * UpgradeSchema constructor.
     *
     * @param ResourceConnection $resourceConnection
     */
    public function __construct(
        ResourceConnection $resourceConnection
    ) {
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * Upgrade the database.
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        // set installer
        $this->installer = $setup;

        // start setup
        $this->installer->startSetup();

        // updates if the module version is lower than 3.0.0
        if (version_compare($context->getVersion(), '3.0.0', '<')) {
            $this->installScalapayOrderToken();
        }

        // end setup
        $this->installer->endSetup();
    }

    /**
     * Installs the Scalapay Order Token columns.
     *
     * @return void
     */
    private function installScalapayOrderToken()
    {
        // set tables
        $tables = ['quote', 'sales_order'];

        // set columns
        $columns = [
            'scalapay_order_token' => [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => 255,
                'comment' =>'Scalapay Order Token'
            ]
        ];

        // loop tables
        foreach ($tables as $table) {
            // get table name considering an eventual prefix
            $table = $this->resourceConnection->getTableName($table);

            // skip if the table does not exist
            if (!$this->installer->getConnection()->isTableExists($table)) {
                continue;
            }

            // add columns if it doesn't already exist
            foreach ($columns as $column => $settings) {
                if (!$this->installer->getConnection()->tableColumnExists($table, $column)) {
                    $this->installer->getConnection()->addColumn(
                        $this->installer->getTable($table),
                        $column,
                        $settings
                    );
                }
            }
        }
    }
}
