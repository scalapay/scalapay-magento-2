<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Controller\Confirm;

use Exception;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\Message\ManagerInterface as MessageManager;
use Magento\Sales\Api\InvoiceOrderInterface;
use Scalapay\Scalapay\Gateway\Settings\Scalapay\Settings as ScalapaySettings;
use Scalapay\Scalapay\Helper\Order as OrderHelper;
use Scalapay\Scalapay\Helper\Payment as PaymentHelper;
use Scalapay\Scalapay\Logger\Logger;
use Scalapay\Scalapay\Model\DelayedCapture;

/**
 * Class Index
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Controller\Confirm
 */
class Index implements ActionInterface
{
    /** @var CheckoutSession $checkoutSession */
    private $checkoutSession;

    /** @var RequestInterface $requestInterface */
    private $requestInterface;

    /** @var RedirectFactory $redirectFactory */
    private $redirectFactory;

    /** @var MessageManager $messageManager */
    private $messageManager;

    /** @var InvoiceOrderInterface $invoiceOrderInterface */
    private $invoiceOrderInterface;

    /** @var ScalapaySettings $scalapaySettings */
    private $scalapaySettings;

    /** @var OrderHelper $orderHelper */
    private $orderHelper;

    /** @var PaymentHelper $paymentHelper */
    private $paymentHelper;

    /** @var Logger $logger */
    private $logger;

    /** @var DelayedCapture $delayedCapture */
    private $delayedCapture;

    /**
     * Index constructor.
     *
     * @param CheckoutSession $checkoutSession
     * @param RequestInterface $requestInterface
     * @param RedirectFactory $redirectFactory
     * @param MessageManager $messageManager
     * @param InvoiceOrderInterface $invoiceOrderInterface
     * @param ScalapaySettings $scalapaySettings
     * @param OrderHelper $orderHelper
     * @param PaymentHelper $paymentHelper
     * @param Logger $logger
     * @param DelayedCapture $delayedCapture
     */
    public function __construct(
        CheckoutSession $checkoutSession,
        RequestInterface $requestInterface,
        RedirectFactory $redirectFactory,
        MessageManager $messageManager,
        InvoiceOrderInterface $invoiceOrderInterface,
        ScalapaySettings $scalapaySettings,
        OrderHelper $orderHelper,
        PaymentHelper $paymentHelper,
        Logger $logger,
        DelayedCapture $delayedCapture
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->requestInterface = $requestInterface;
        $this->redirectFactory = $redirectFactory;
        $this->messageManager = $messageManager;
        $this->invoiceOrderInterface = $invoiceOrderInterface;
        $this->scalapaySettings = $scalapaySettings;
        $this->orderHelper = $orderHelper;
        $this->paymentHelper = $paymentHelper;
        $this->logger = $logger;
        $this->delayedCapture = $delayedCapture;
    }

    /**
     * Creates order and capture.
     *
     * @return Redirect
     */
    public function execute(): Redirect
    {
        try {
            // log
            $this->logger->info(__CLASS__ . ': Start confirm controller');

            // get and check order token
            $orderToken = $this->requestInterface->getParam('orderToken');
            if (!$orderToken) {
                throw new Exception('Order token is empty.');
            }

            // log
            $this->logger->info(__CLASS__ . ': Order token: ' . $orderToken);

            // get quote by order token
            $quote = $this->orderHelper->getQuoteByOrderToken($orderToken);

            // log
            $this->logger->info(
                __CLASS__ . ': Retrieved quote with id: ' . $quote->getId() .
                ' from order token: ' . $orderToken
            );

            // get and check payment method
            $paymentMethod = $quote->getPayment()->getMethod();
            if (!$this->paymentHelper->isScalapayPayment($paymentMethod)) {
                throw new Exception(
                    'Invalid payment method' .
                    ' - Quote Id: ' . $quote->getId() .
                    ' - Order token: ' . $orderToken .
                    ' - Payment method: ' . $paymentMethod
                );
            }

            // log
            $this->logger->info(
                __CLASS__ . ': Payment method: ' . $paymentMethod .
                ' - Quote Id: ' . $quote->getId()
            );

            // check if the payment is authorized
            $paymentIsAuthorized = $this->orderHelper->paymentIsAuthorized($quote);
            if (!$paymentIsAuthorized) {
                throw new Exception('Payment is not authorized - Quote Id: ' . $quote->getId());
            }

            // log
            $this->logger->info(
                __CLASS__ . ': Payment has been authorized by Scalapay' .
                ' - Quote Id: ' . $quote->getId() .
                ' - Order token: ' . $orderToken
            );

            // place the order into Magento
            $this->checkoutSession->setIsScalapayOrderCreation(true);
            $orderId = $this->orderHelper->placeOrder($quote);
            $this->checkoutSession->setIsScalapayOrderCreation(false);

            // log
            $this->logger->info(
                __CLASS__ . ': Order has been placed with id: ' . $orderId .
                ' from quote id: ' . $quote->getId()
            );

            // capture with delay mode or standard mode
            $delayCaptureIsActive = $this->scalapaySettings->getDelayedCapture();
            if ($delayCaptureIsActive) {
                // log
                $this->logger->info(
                    __CLASS__ . ': Delay capture is enabled' .
                    ' - Quote Id: ' . $quote->getId() .
                    ' - Order Id: ' . $orderId
                );

                // extends payment authorization and not capture for the moment
                $this->delayedCapture->execute($orderId);
            } else {
                // log
                $this->logger->info(
                    __CLASS__ . ': Delay capture is not enabled: Create the invoice and capture the payment' .
                    ' - Quote Id: ' . $quote->getId() .
                    ' - Order Id: ' . $orderId
                );

                // create invoice and capture
                $this->checkoutSession->setIsScalapayOrderInvoice(true);
                $this->invoiceOrderInterface->execute($orderId, true);
                $this->checkoutSession->setIsScalapayOrderInvoice(false);
            }

            // log
            $this->logger->info(
                __CLASS__ . ': Redirect customer to success page' .
                ' - Quote Id: ' . $quote->getId() .
                ' - Order Id: ' . $orderId
            );

            // redirect to success page
            $redirect = $this->redirectFactory->create();
            return $redirect->setPath(
                $this->scalapaySettings->getOrderSuccessRedirect(),
                ['_secure' => true]
            );
        } catch (Exception $e) {
            // log exception
            $this->logger->critical(__CLASS__ . ': ' . $e->getMessage());

            // set session flags to false on the checkout session
            $this->checkoutSession->setIsScalapayOrderCreation(false);
            $this->checkoutSession->setIsScalapayOrderInvoice(false);

            // remove reserved order id from quote
            $orderToken = $this->requestInterface->getParam('orderToken');
            $this->orderHelper->unsetReserveOrderIdByOrderToken($orderToken);

            // set a generic error message
            $errorMessage = 'Internal server error. Please contact us or try again.';
            $this->messageManager->addErrorMessage(__($errorMessage));

            // log
            $this->logger->info(__CLASS__ . ': Redirect customer to failure page');

            // redirect to failure page
            $redirect = $this->redirectFactory->create();
            return $redirect->setPath(
                $this->scalapaySettings->getOrderFailureRedirect(),
                ['_secure' => true]
            );
        }
    }
}
