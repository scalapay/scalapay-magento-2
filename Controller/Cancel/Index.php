<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Controller\Cancel;

use Magento\Framework\App\ActionInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\Result\RedirectFactory;
use Scalapay\Scalapay\Gateway\Settings\Scalapay\Settings as ScalapaySettings;
use Scalapay\Scalapay\Logger\Logger;

/**
 * Class Index
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Controller\Redirect
 */
class Index implements ActionInterface
{
    /** @var RedirectFactory $redirectFactory */
    private $redirectFactory;

    /** @var ScalapaySettings $scalapaySettings */
    private $scalapaySettings;

    /** @var Logger $logger */
    private $logger;

    /**
     * Index constructor.
     *
     * @param RedirectFactory $redirectFactory
     * @param ScalapaySettings $scalapaySettings
     * @param Logger $logger
     */
    public function __construct(
        RedirectFactory $redirectFactory,
        ScalapaySettings $scalapaySettings,
        Logger $logger
    ) {
        $this->redirectFactory = $redirectFactory;
        $this->scalapaySettings = $scalapaySettings;
        $this->logger = $logger;
    }

    /**
     * Redirects the customer to the checkout cart.
     *
     * @return Redirect
     */
    public function execute(): Redirect
    {
        // log
        $this->logger->info(__CLASS__ . ': Redirect customer to cart page.');

        // redirect customer to cart page
        $redirect = $this->redirectFactory->create();
        return $redirect->setPath(
            $this->scalapaySettings->getCheckoutCartRedirect(),
            ['_secure' => true]
        );
    }
}
