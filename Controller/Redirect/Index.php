<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Controller\Redirect;

use Exception;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Message\ManagerInterface as MessageManagerInterface;
use Magento\Framework\UrlInterface;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Api\Data\CartInterface;
use Scalapay\Scalapay\Gateway\Settings\Scalapay\Settings as ScalapaySettings;
use Scalapay\Scalapay\Helper\Payment as PaymentHelper;
use Scalapay\Scalapay\Logger\Logger;
use Scalapay\Scalapay\Model\ApiClient;
use Scalapay\Scalapay\Model\OrderDetail;
use Scalapay\Scalapay\Model\ResourceModel\OrderToken;

/**
 * Class Index
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Controller\Redirect
 */
class Index implements ActionInterface
{
    /** @var CheckoutSession $checkoutSession */
    private $checkoutSession;

    /** @var RequestInterface $requestInterface */
    private $requestInterface;

    /** @var RedirectFactory $redirectFactory */
    private $redirectFactory;

    /** @var ResultFactory $resultFactory */
    private $resultFactory;

    /** @var MessageManagerInterface $messageManagerInterface */
    private $messageManagerInterface;

    /** @var UrlInterface $urlInterface */
    private $urlInterface;

    /** @var CartRepositoryInterface $cartRepositoryInterface */
    private $cartRepositoryInterface;

    /** @var ScalapaySettings $scalapaySettings */
    private $scalapaySettings;

    /** @var PaymentHelper $paymentHelper */
    private $paymentHelper;

    /** @var Logger $logger */
    private $logger;

    /** @var ApiClient $apiClient */
    private $apiClient;

    /** @var OrderDetail $orderDetail */
    private $orderDetail;

    /** @var OrderToken $orderToken */
    private $orderToken;

    /**
     * Index constructor.
     *
     * @param CheckoutSession $checkoutSession
     * @param RequestInterface $requestInterface
     * @param RedirectFactory $redirectFactory
     * @param ResultFactory $resultFactory
     * @param MessageManagerInterface $messageManagerInterface
     * @param UrlInterface $urlInterface
     * @param CartRepositoryInterface $cartRepositoryInterface
     * @param ScalapaySettings $scalapaySettings
     * @param PaymentHelper $paymentHelper
     * @param Logger $logger
     * @param ApiClient $apiClient
     * @param OrderDetail $orderDetail
     * @param OrderToken $orderToken
     */
    public function __construct(
        CheckoutSession $checkoutSession,
        RequestInterface $requestInterface,
        RedirectFactory $redirectFactory,
        ResultFactory $resultFactory,
        MessageManagerInterface $messageManagerInterface,
        UrlInterface $urlInterface,
        CartRepositoryInterface $cartRepositoryInterface,
        ScalapaySettings $scalapaySettings,
        PaymentHelper $paymentHelper,
        Logger $logger,
        ApiClient $apiClient,
        OrderDetail $orderDetail,
        OrderToken $orderToken
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->requestInterface = $requestInterface;
        $this->redirectFactory = $redirectFactory;
        $this->resultFactory = $resultFactory;
        $this->messageManagerInterface = $messageManagerInterface;
        $this->urlInterface = $urlInterface;
        $this->cartRepositoryInterface = $cartRepositoryInterface;
        $this->scalapaySettings = $scalapaySettings;
        $this->paymentHelper = $paymentHelper;
        $this->logger = $logger;
        $this->apiClient = $apiClient;
        $this->orderDetail = $orderDetail;
        $this->orderToken = $orderToken;
    }

    /**
     * Redirects the customer to the Scalapay checkout portal.
     *
     * @return ResultInterface
     */
    public function execute(): ResultInterface
    {
        try {
            // log
            $this->logger->info(__CLASS__ . ': Start redirect controller');

            // get quote id from checkout session
            $quoteId = $this->checkoutSession->getQuoteId();
            if (!$quoteId) {
                throw new Exception('Invalid quote id.');
            }

            // log
            $this->logger->info(__CLASS__ . ': Quote id: ' . $quoteId);

            // get quote
            $quote = $this->cartRepositoryInterface->get($quoteId);

            // check quote
            if (!$quote || !$quote->getId()) {
                throw new Exception('Invalid quote - Quote id: ' . $quoteId);
            }

            if (!$quote->getIsActive()) {
                throw new Exception('Invalid quote: Quote id ' . $quoteId . ' is not active.');
            }

            // log
            $this->logger->info(__CLASS__ . ': Quote with id ' . $quoteId . ' is valid.');

            // get redirect url
            $redirectUrl = $this->getRedirectUrl($quote);
            if (!$redirectUrl) {
                throw new Exception('Invalid redirect url - Quote id: ' . $quoteId);
            }

            // log
            $this->logger->info(__CLASS__ . ': Redirect url: ' . $redirectUrl . ' - Quote id: ' . $quoteId);

            // return success response
            return $this->response($redirectUrl);
        } catch (Exception $e) {
            // log exception
            $this->logger->critical(__CLASS__ . ': ' . $e->getMessage());

            // return error response with a generic error message
            $redirectUrl = $this->urlInterface->getUrl($this->scalapaySettings->getCheckoutCartRedirect());
            return $this->response($redirectUrl, true);
        }
    }

    /**
     * Returns the Scalapay checkout url designed for the redirect.
     *
     * @param CartInterface $quote
     * @return string
     * @throws Exception
     */
    private function getRedirectUrl(
        CartInterface $quote
    ): string {
        // get and check quote id
        $quoteId = (int) $quote->getId();
        if (!$quoteId) {
            throw new Exception('Invalid quote id.');
        }

        // get payment method
        $paymentMethod = $quote->getPayment()->getMethod();

        // get order details
        $orderDetails = $this->orderDetail->execute($quote);

        // get api client
        $apiClient = $this->apiClient->execute($paymentMethod);

        // create Scalapay order
        $scalapayOrder = $apiClient->createOrder($orderDetails);

        // get and check order token
        $orderToken = $scalapayOrder->getToken();
        if (!$orderToken) {
            throw new Exception('Invalid order token - Quote id: ' . $quoteId);
        }

        // save order token on the quote table
        $saveOrderToken = $this->orderToken->saveOnQuoteTable($quoteId, $orderToken);
        if (!$saveOrderToken) {
            throw new Exception('Impossible save the order token on quote - Quote id: ' . $quoteId);
        }

        $checkoutUrl = $scalapayOrder->getCheckoutUrl();
        if (!$checkoutUrl) {
            throw new Exception('Invalid Scalapay checkout redirect url - Quote id: ' . $quoteId);
        }

        // return the Scalapay checkout url
        return $checkoutUrl;
    }

    /**
     * Returns the controller response.
     *
     * @param string $redirectUrl
     * @param bool $isException
     * @return Json|Redirect
     */
    private function response(
        string $redirectUrl,
        bool $isException = false
    ) {
        // set generic error message
        $errorMessage = 'Internal server error. Please contact us or try again.';

        // set log string array
        $logStrings = [
            'quoteId' => $this->checkoutSession->getQuoteId() ? $this->checkoutSession->getQuoteId() : 'N/A',
            'isException' => $isException ? 'true' : 'false',
            'errorMessage' => $isException ? $errorMessage : 'N/A'
        ];

        // manage ajax call for in page checkout
        if ($this->requestInterface->isAjax()) {
            // log
            $this->logger->info(
                __CLASS__ . ': Call type: AJAX' .
                ' - Quote Id: ' . $logStrings['quoteId'] .
                ' - isException: ' . $logStrings['isException'] .
                ' - errorMessage: ' . $logStrings['errorMessage']
            );

            // json response
            $jsonResponse = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            return $jsonResponse->setData([
                'redirect' => $redirectUrl,
                'result' => !$isException,
                'messages' => $isException ? __($errorMessage) : ''
            ]);
        }

        // log
        $this->logger->info(
            __CLASS__ . ': Call type: Redirect' .
            ' - Quote Id: ' . $logStrings['quoteId'] .
            ' - isException: ' . $logStrings['isException'] .
            ' - errorMessage: ' . $logStrings['errorMessage']
        );

        // redirect response
        if ($isException) {
            $this->messageManagerInterface->addErrorMessage(__($errorMessage));
        }
        $redirect = $this->redirectFactory->create();
        return $redirect->setUrl($redirectUrl);
    }
}
