<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Observer;

use Exception;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\Order;
use Scalapay\Scalapay\Gateway\Settings\Scalapay\Settings as ScalapaySettings;
use Scalapay\Scalapay\Helper\Order as OrderHelper;
use Scalapay\Scalapay\Helper\Payment as PaymentHelper;
use Scalapay\Scalapay\Logger\Logger;

/**
 * Class UpdateOrderStatus
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Observer
 */
class UpdateOrderStatus implements ObserverInterface
{
    /** @var string ORDER_CREATION_PHASE */
    const ORDER_CREATION_PHASE = 'order creation';

    /** @var string ORDER_INVOICE_PHASE */
    const ORDER_INVOICE_PHASE = 'order invoice';

    /** @var CheckoutSession $checkoutSession */
    private $checkoutSession;

    /** @var ScalapaySettings $scalapaySettings */
    private $scalapaySettings;

    /** @var OrderHelper $orderHelper */
    private $orderHelper;

    /** @var PaymentHelper $paymentHelper */
    private $paymentHelper;

    /** @var Logger $logger */
    private $logger;

    /**
     * UpdateOrderStatus constructor.
     *
     * @param CheckoutSession $checkoutSession
     * @param ScalapaySettings $scalapaySettings
     * @param OrderHelper $orderHelper
     * @param PaymentHelper $paymentHelper
     * @param Logger $logger
     */
    public function __construct(
        CheckoutSession $checkoutSession,
        ScalapaySettings $scalapaySettings,
        OrderHelper $orderHelper,
        PaymentHelper $paymentHelper,
        Logger $logger
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->scalapaySettings = $scalapaySettings;
        $this->orderHelper = $orderHelper;
        $this->paymentHelper = $paymentHelper;
        $this->logger = $logger;
    }

    /**
     * Updates the order status by the given phase from session flags.
     *
     * @param Observer $observer
     * @return UpdateOrderStatus
     */
    public function execute(Observer $observer): UpdateOrderStatus
    {
        try {
            // get and check the order
            /** @var Order $order */
            $order = $observer->getData('order');
            if (!$order) {
                return $this;
            }

            // skip if it is not a Scalapay payment method
            $paymentMethod = $order->getPayment()->getMethod();
            if (!$this->paymentHelper->isScalapayPayment($paymentMethod)) {
                return $this;
            }

            // log
            $this->logger->info(
                __CLASS__ . ': Updating state and status' .
                ' - OrderId: ' . ($order->getId() ? $order->getId() : 'N/A') .
                ' - QuoteId: ' . ($order->getQuoteId() ? $order->getQuoteId() : 'N/A')
            );

            // update order status after order creation
            $isScalapayOrderCreation = $this->checkoutSession->getIsScalapayOrderCreation();
            $isOrderCreationPhase = $isScalapayOrderCreation && !$order->hasInvoices();
            if ($isOrderCreationPhase) {
                // update status with authorize status
                $status = $this->scalapaySettings->getAuthorizeOrderStatus();
                $this->updateStatus($order, $status, self::ORDER_CREATION_PHASE);

                // return
                return $this;
            }

            // update order status after order invoice creation
            $isScalapayOrderInvoice = $this->checkoutSession->getIsScalapayOrderInvoice();
            $isOrderInvoicePhase = $isScalapayOrderInvoice && $order->hasInvoices();
            if ($isOrderInvoicePhase) {
                // update status with capture status
                $status = $this->scalapaySettings->getCaptureOrderStatus();
                $this->updateStatus($order, $status, self::ORDER_INVOICE_PHASE);

                // return
                return $this;
            }

            // log unexpected condition
            if ($isScalapayOrderInvoice && !$order->hasInvoices()) {
                throw new Exception('Unexpected condition on update status order.');
            }

            // return
            return $this;
        } catch (Exception $e) {
            $this->logger->critical(__CLASS__ . ': ' . $e->getMessage());
            return $this;
        }
    }

    /**
     * Update the order status.
     *
     * @param Order $order
     * @param string $status
     * @param string $stringPhase
     * @return void
     * @throws Exception
     */
    private function updateStatus(Order $order, string $status, string $stringPhase): void
    {
        // get state by status
        $state = $this->orderHelper->getStateByStatus($status);

        // check status and state
        if (!$status || !$state) {
            throw new Exception('Invalid status or state - State: ' . $state . ' - Status: ' . $status);
        }

        // update order state and order status
        $order->setState($state)
            ->setStatus($status);

        // log
        $this->logger->info(
            __CLASS__ . ': State and status updated on ' . $stringPhase .
            ' - OrderId: ' . ($order->getId() ? $order->getId() : 'N/A') .
            ' - QuoteId: ' . ($order->getQuoteId() ? $order->getQuoteId() : 'N/A') .
            ' - State: '. $state .
            ' - Status: '. $status
        );
    }
}
