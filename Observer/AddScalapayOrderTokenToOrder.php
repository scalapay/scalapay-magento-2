<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Observer;

use Exception;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Scalapay\Scalapay\Helper\Payment as PaymentHelper;
use Scalapay\Scalapay\Logger\Logger;

/**
 * Class AddScalapayOrderTokenToOrder
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Observer
 */
class AddScalapayOrderTokenToOrder implements ObserverInterface
{
    /** @var PaymentHelper $paymentHelper */
    private $paymentHelper;

    /** @var Logger $logger */
    private $logger;

    /**
     * AddScalapayOrderTokenToOrder constructor.
     *
     * @param PaymentHelper $paymentHelper
     * @param Logger $logger
     */
    public function __construct(
        PaymentHelper $paymentHelper,
        Logger $logger
    ) {
        $this->paymentHelper = $paymentHelper;
        $this->logger = $logger;
    }

    /**
     * Copies Scalapay order token from quote entity to order entity.
     *
     * @param Observer $observer
     * @return $this
     */
    public function execute(Observer $observer): AddScalapayOrderTokenToOrder
    {
        try {
            // get and check the quote
            $quote = $observer->getData('quote');
            if (!$quote || !$quote->getId()) {
                return $this;
            }

            // skip if it is not a Scalapay payment method
            $paymentMethod = $quote->getPayment()->getMethod();
            if (!$this->paymentHelper->isScalapayPayment($paymentMethod)) {
                return $this;
            }

            // get scalapay order token from quote
            $scalapayOrderToken = $quote->getScalapayOrderToken();
            if (!$scalapayOrderToken) {
                throw new Exception('Scalapay order token is empty.');
            }

            // save scalapay order token on the order
            $order = $observer->getData('order');
            $order->setScalapayOrderToken($scalapayOrderToken);

            // return
            return $this;
        } catch (Exception $e) {
            $this->logger->critical(__CLASS__ . ': ' . $e->getMessage());
            return $this;
        }
    }
}
