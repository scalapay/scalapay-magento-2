# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [4.0.3] - 2025-02-14
### Fixed
- [IN-5241] - Solves a console error when the Scalapay Widget is hidden on the page
- [IN-5241] - Solves a visualization issue of the Scalapay Widget for Magento versions lower than 2.4.0

## [4.0.2] - 2025-02-11
### Changed
- [IN-5241] - Retrieves redirect urls from hidden configs
### Fixed
- [SP-21519] - Fixes Scalapay widget at checkout in M2.4.7 or greater

## [4.0.1] - 2025-01-13
### Fixed
- [SP-20627] - Minor adjustments on the migration script

## [4.0.0] - 2025-01-10
### Changed
- [SP-20624] - Use a single configuration for all Scalapay products
### Removed
- [SP-20624] - Removed In Page Checkout feature

## [3.5.1] - 2024-12-04
### Changed
- [IN-5185] - Removes currency configurations

## [3.5.0] - 2024-11-07
### Added
- [SP-19338] - Implements Scalapay Product Suite
- [SP-19821] - Adds split fee configuration

## [3.4.1] - 2024-10-17
### Fixed
- [IN-5154] - Adds minor fix on Order helper

## [3.4.0] - 2024-09-18
### Changed
- [IN-5117] - Implements widget loader script
### Fixed
- [IN-5127] - Fixes a minor issue on the visualization of the widget

## [3.3.8] - 2024-09-17
### Fixed
- [IN-5129] - Adds a fix on the retrieve of table names with a prefix

## [3.3.7] - 2024-08-02
### Fixed
- [IN-5092] - Adds a minor fix on French translations

## [3.3.6] - 2024-06-26
### Fixed
- [IN-5058] - Improves paymentIsAuthorized method (total amount mismatch check)

## [3.3.5] - 2024-06-24
### Fixed
- [IN-5048] - Fix support for Adobe Commerce and Magento Enterprise editions

## [3.3.4] - 2024-04-17
### Fixed
- [IN-4976] - Removes query log from CancelOrders cron

## [3.3.3] - 2024-02-09
### Fixed
- [IN-4836] - Improves loading performances on the Payment Methods page

## [3.3.2] - 2024-02-01
### Fixed
- [IN-4852] - Solves configuration retrieved by store view issues
### Changed
- [IN-4852] - Permits to set API keys and mode in store views
- [IN-4852] - Permits to set authorize - capture - invalid order statuses configuration in default view only

## [3.3.1] - 2023-12-19
### Changed
- [IN-4790] - Permits to enable Scalapay by store view also

## [3.3.0] - 2023-12-04
### Added
- [IN-4612] - Implements Risk data to Extensions Data on Create Order API
- [IN-4612] - Implements 'Enable Extra Merchant Data' configuration
### Changed
- [IN-4714] - Casts amount values to float on Order Create and Order Refund
- [IN-4714] - Makes package dependency more restrictive
### Fixed
- [IN-4736] - Adds merchant reference on capture API call
- [IN-4750] - Adjusts CHANGELOG.md file
### Removed
- [IN-4762] - Removes Scalapay Pay Later payment method

## [3.2.4] - 2023-11-17
### Fixed
- [IN-4602] - Fixes upgrade schema setup script

## [3.2.3] - 2023-11-08
### Fixed
- [IN-4698] - Fixes table prefix issue on CancelOrders cron and UpgradeSchema setup file

## [3.2.2] - 2023-10-18
### Fixed
- [IN-4658] - Solves API Client production mode issues
- [IN-4658] - Solves bad query on Cancel Orders cron

## [3.2.1] - 2023-10-13
### Fixed
- [IN-4646] - Applies a minor fix on refund flow
- [IN-4646] - Adjusts CHANGELOG.md
### Changed
- [IN-4646] - Improves exception management

## [3.2.0] - 2023-10-10
### Changed
- [IN-4615] - Upgrades Scalapay PHP SDK

## [3.1.0] - 2023-09-26
### Changed
- [IN-4534] - Implements Scalapay SDK v3.0

## [3.0.2] - 2023-09-18
### Fixed
- [IN-4573] - Solves inheritance configuration issue on save

### Changed
- [IN-4573] - Updates composer.json php require version to >= 7.1

## [3.0.1] - 2023-09-07
### Fixed
- [IN-4454] - Improves In Page Checkout redirection flow
- [IN-4454] - Solves conflict with In Page Checkout observer
- [IN-4454] - Updates README.md

## [3.0.0] - 2023-09-05
### Added
- [IN-4301] - Implements ACL
- [IN-4304] - Adds logs up to the previous six months
- [IN-4304] - Adds retro-compatibility since Magento 2.2.0
- [IN-4305] - Adds configuration to set different order statuses depending on the authorization - capture - cancel phases
- [IN-4305] - Adds a cron to move bad orders (expired or failed on Scalapay side) in canceled status or other specified by the related configuration
- [IN-4305] - Code optimization
### Fixed
- [IN-4301] - Fixes logic for min and max amount in frontend widget visualization
- [IN-4301] - Fixes frontend widget visualization checks on Pay In 4 and Pay Later methods
- [IN-4301] - Fixes frontend widget visualization delay
- [IN-4301] - Fixes Widget visualization stability
- [IN-4304] - Fixes Controllers stability
- [IN-4305] - Fixes Order flow stability
### Removed
- [IN-4304] - Removes Guzzle and substituted with an integrated curl service
- [IN-4305] - Removes In Page Checkout in Magento versions that doesnt support it (<= 2.4.3)
