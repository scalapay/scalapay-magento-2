/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
define([
    'Magento_Checkout/js/view/payment/default',
    'ko',
    'mage/url',
    'Magento_Checkout/js/model/quote'
], function (Component, ko, url, quote) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Scalapay_Scalapay/payment/scalapay'
        },

        redirectAfterPlaceOrder: false,

        title: ko.observable(''),

        /**
         * Initialize Scalapay Product Suite payment method.
         */
        initialize: function () {
            this._super();

            quote.totals.subscribe(this.updateTitle.bind(this));
            this.updateTitle(this);
        },

        /**
         * Updates Scalapay payment method title.
         */
        updateTitle: function () {
            const config = window.checkoutConfig.payment.scalapay;
            const logo = config.logo;
            const title = config.title.replace('[logo]', logo);
            this.title(title);
        },

        /**
         * Redirect after place order.
         */
        afterPlaceOrder: function () {
            window.location.replace(url.build('scalapay/redirect/index'));
        }
    });
});
