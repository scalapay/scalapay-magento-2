/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
define([
    'uiComponent',
    'Magento_Checkout/js/model/payment/renderer-list'
], function (Component, rendererList) {
    'use strict';

    rendererList.push(
        {
            type: 'scalapay',
            component: 'Scalapay_Scalapay/js/view/payment/method-renderer/scalapay'
        }
    );

    return Component.extend({});
});
