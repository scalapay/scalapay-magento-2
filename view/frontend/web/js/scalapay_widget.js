document.addEventListener('DOMContentLoaded', () => {
    // instance scalapay widget observer
    const scalapayWidgetObserver = new MutationObserver(() => {
        // disable observer to avoid infinite loops caused by DOM changes
        scalapayWidgetObserver.disconnect();

        // get scalapay widget configurations
        const widgetConfigurations = JSON.parse(
            document.querySelector('#scalapayWidgetConfigurations')?.textContent || '{}'
        );

        // loop each scalapay product configuration
        if (Object.keys(widgetConfigurations).length) {
            // check if amount selector exists
            let amountSelectorExists = document.querySelector(JSON.parse(widgetConfigurations.amountSelectors));
            if (amountSelectorExists) {
                // get and check widget element
                const widgetElement = document.querySelector(widgetConfigurations.productWidgetId);
                if (widgetElement) {
                    // manage widget in product view and cart page
                    if (widgetConfigurations.sectionType === 'product' || widgetConfigurations.sectionType === 'cart') {
                        // get and check reference node
                        const referenceNode = document.querySelector(widgetConfigurations.textPosition);
                        if (referenceNode) {
                            // add the widget if it hasn't been added yet
                            if (!referenceNode.parentNode.contains(widgetElement)) {
                                // insert widget
                                referenceNode.parentNode.insertBefore(widgetElement, referenceNode.nextSibling);

                                // show the widget
                                document.querySelector(widgetConfigurations.productWidgetId).style.visibility = 'visible';
                            }
                        }
                    }

                    // manage widget into the checkout
                    if (widgetConfigurations.sectionType === 'checkout') {
                        // get and check reference
                        const referenceNode = document.querySelector('#scalapay-widget-container');
                        if (referenceNode) {
                            // add the widget if it hasn't been added yet
                            if (!referenceNode.contains(widgetElement)) {
                                // insert widget
                                referenceNode.appendChild(widgetElement);

                                // show the widget
                                document.querySelector(widgetConfigurations.productWidgetId).style.visibility = 'visible';
                            }
                        }
                    }
                }
            }
        }

        // enable again observer
        scalapayWidgetObserver.observe(document, {
            attributes: true,
            childList: true,
            characterData: true,
            subtree: true
        });
    });

    // instance observer
    scalapayWidgetObserver.observe(document, {
        attributes: true,
        childList: true,
        characterData: true,
        subtree: true
    });
});
