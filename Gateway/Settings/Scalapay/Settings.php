<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Gateway\Settings\Scalapay;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Payment\Gateway\Config\Config as GatewayConfig;

/**
 * Class Settings
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Gateway\Settings\Scalapay
 */
class Settings extends GatewayConfig
{
    /** @var string CODE */
    const CODE = 'scalapay';

    /** @var string ACTIVE */
    const ACTIVE = 'active';

    /** @var string MODE */
    const MODE = 'general/mode';

    /** @var string TEST_URL */
    const TEST_URL = 'general/test_url';

    /** @var string TEST_MERCHANT_TOKEN */
    const TEST_MERCHANT_TOKEN = 'general/test_merchant_token';

    /** @var string TEST_API_KEY */
    const TEST_API_KEY = 'general/test_api_key';

    /** @var string PRODUCTION_URL */
    const PRODUCTION_URL = 'general/production_url';

    /** @var string PRODUCTION_MERCHANT_TOKEN */
    const PRODUCTION_MERCHANT_TOKEN = 'general/production_merchant_token';

    /** @var string PRODUCTION_API_KEY */
    const PRODUCTION_API_KEY = 'general/production_api_key';

    /** @var string DELAYED_CAPTURE */
    const DELAYED_CAPTURE = 'general/delayed_capture';

    /** @var string ENABLE_EXTRA_MERCHANT_DATA */
    const ENABLE_EXTRA_MERCHANT_DATA = 'general/enable_extra_merchant_data';

    /** @var string CHECKOUT_CART_REDIRECT */
    const CHECKOUT_CART_REDIRECT = 'general/checkout_cart_redirect';

    /** @var string ORDER_SUCCESS_REDIRECT */
    const ORDER_SUCCESS_REDIRECT = 'general/order_success_redirect';

    /** @var string ORDER_FAILURE_REDIRECT */
    const ORDER_FAILURE_REDIRECT = 'general/order_failure_redirect';

    /** @var string MIN_AMOUNT */
    const MIN_AMOUNT = 'payment_configurations/min_amount';

    /** @var string MAX_AMOUNT */
    const MAX_AMOUNT = 'payment_configurations/max_amount';

    /** @var string DISABLED_CATEGORIES */
    const DISABLED_CATEGORIES = 'payment_configurations/disabled_categories';

    /** @var string DISABLED_PRODUCT_TYPES */
    const DISABLED_PRODUCT_TYPES = 'payment_configurations/disabled_product_types';

    /** @var string DISABLED_COUNTRIES */
    const DISABLED_COUNTRIES = 'payment_configurations/disabled_countries';

    /** @var string DISABLED_LANGUAGES */
    const DISABLED_LANGUAGES = 'payment_configurations/disabled_languages';

    /** @var string AUTHORIZE_ORDER_STATUS */
    const AUTHORIZE_ORDER_STATUS = 'order_statuses/authorize_order_status';

    /** @var string CAPTURE_ORDER_STATUS */
    const CAPTURE_ORDER_STATUS = 'order_statuses/capture_order_status';

    /** @var string INVALID_ORDER_STATUS */
    const INVALID_ORDER_STATUS = 'order_statuses/invalid_order_status';

    /**
     * Settings constructor.
     *
     * @param ScopeConfigInterface $scopeConfig
     * @param null $methodCode
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        $methodCode = null
    ) {
        parent::__construct($scopeConfig, $methodCode);
    }

    /**
     * Returns the code.
     *
     * @return string
     */
    public function getCode(): string
    {
        return self::CODE;
    }

    /**
     * Returns if it is active.
     *
     * @param int|null $storeId
     * @return bool
     */
    public function isActive(?int $storeId = null): bool
    {
        return (bool) $this->getValue(self::ACTIVE, $storeId);
    }

    /**
     * Returns if it is live mode.
     *
     * @param int|null $storeId
     * @return bool
     */
    public function getLiveMode(?int $storeId = null): bool
    {
        return (bool) $this->getValue(self::MODE, $storeId);
    }

    /**
     * Returns the test url.
     *
     * @param int|null $storeId
     * @return string
     */
    public function getTestUrl(?int $storeId = null): string
    {
        return $this->getValue(self::TEST_URL, $storeId);
    }

    /**
     * Returns the test merchant token.
     *
     * @param int|null $storeId
     * @return string
     */
    public function getTestMerchantToken(?int $storeId = null): string
    {
        return $this->getValue(self::TEST_MERCHANT_TOKEN, $storeId);
    }

    /**
     * Returns the test api key.
     *
     * @param int|null $storeId
     * @return string
     */
    public function getTestApiKey(?int $storeId = null): string
    {
        return $this->getValue(self::TEST_API_KEY, $storeId);
    }

    /**
     * Returns the production url.
     *
     * @param int|null $storeId
     * @return string
     */
    public function getProductionUrl(?int $storeId = null): string
    {
        return $this->getValue(self::PRODUCTION_URL, $storeId);
    }

    /**
     * Returns the production merchant token.
     *
     * @param int|null $storeId
     * @return string
     */
    public function getProductionMerchantToken(?int $storeId = null): string
    {
        return $this->getValue(self::PRODUCTION_MERCHANT_TOKEN, $storeId);
    }

    /**
     * Returns the production api key.
     *
     * @param int|null $storeId
     * @return string|null
     */
    public function getProductionApiKey(?int $storeId = null): ?string
    {
        return $this->getValue(self::PRODUCTION_API_KEY, $storeId);
    }

    /**
     * Returns true if delayed capture is enabled else false.
     *
     * @param int|null $storeId
     * @return bool
     */
    public function getDelayedCapture(?int $storeId = null): bool
    {
        return (bool) $this->getValue(self::DELAYED_CAPTURE, $storeId);
    }

    /**
     * Returns true if extra merchant data is enable else false.
     *
     * @param int|null $storeId
     * @return bool
     */
    public function getEnableExtraMerchantData(?int $storeId = null): bool
    {
        return (bool) $this->getValue(self::ENABLE_EXTRA_MERCHANT_DATA, $storeId);
    }

    /**
     * Returns checkout cart redirect path.
     *
     * @param int|null $storeId
     * @return string
     */
    public function getCheckoutCartRedirect(?int $storeId = null): string
    {
        return $this->getValue(self::CHECKOUT_CART_REDIRECT, $storeId);
    }

    /**
     * Returns order success redirect path.
     *
     * @param int|null $storeId
     * @return string
     */
    public function getOrderSuccessRedirect(?int $storeId = null): string
    {
        return $this->getValue(self::ORDER_SUCCESS_REDIRECT, $storeId);
    }

    /**
     * Returns order failure redirect path.
     *
     * @param int|null $storeId
     * @return string
     */
    public function getOrderFailureRedirect(?int $storeId = null): string
    {
        return $this->getValue(self::ORDER_FAILURE_REDIRECT, $storeId);
    }

    /**
     * Returns the min amount.
     *
     * @param int|null $storeId
     * @return float
     */
    public function getMinAmount(?int $storeId = null): float
    {
        return (float) $this->getValue(self::MIN_AMOUNT, $storeId);
    }

    /**
     * Returns the max amount.
     *
     * @param int|null $storeId
     * @return float
     */
    public function getMaxAmount(?int $storeId = null): float
    {
        return (float) $this->getValue(self::MAX_AMOUNT, $storeId);
    }

    /**
     * Returns the disabled categories as array.
     *
     * @param int|null $storeId
     * @return int[]
     */
    public function getDisabledCategories(?int $storeId = null): array
    {
        $disabledCategories = (string) $this->getValue(self::DISABLED_CATEGORIES, $storeId);
        if (!$disabledCategories) {
            return [];
        }

        return explode(',', $disabledCategories);
    }

    /**
     * Returns the disabled product types as array.
     *
     * @param int|null $storeId
     * @return string[]
     */
    public function getDisabledProductTypes(?int $storeId = null): array
    {
        $disabledProductTypes = (string) $this->getValue(self::DISABLED_PRODUCT_TYPES, $storeId);
        if (!$disabledProductTypes) {
            return [];
        }

        return explode(',', $disabledProductTypes);
    }

    /**
     * Returns the disabled countries as array.
     *
     * @return string[]
     */
    public function getDisabledCountries(?int $storeId = null): array
    {
        $disabledCountries = (string) $this->getValue(self::DISABLED_COUNTRIES, $storeId);
        if (!$disabledCountries) {
            return [];
        }

        return explode(',', $disabledCountries);
    }

    /**
     * Returns the disabled languages as array.
     *
     * @param int|null $storeId
     * @return string[]
     */
    public function getDisabledLanguages(?int $storeId = null): array
    {
        $disabledLanguages = (string) $this->getValue(self::DISABLED_LANGUAGES, $storeId);
        if (!$disabledLanguages) {
            return [];
        }

        return explode(',', $disabledLanguages);
    }
    
    /**
     * Returns authorize order status.
     *
     * @param int|null $storeId
     * @return string
     */
    public function getAuthorizeOrderStatus(?int $storeId = null): string
    {
        return $this->getValue(self::AUTHORIZE_ORDER_STATUS, $storeId);
    }

    /**
     * Returns capture order status.
     *
     * @param int|null $storeId
     * @return string
     */
    public function getCaptureOrderStatus(?int $storeId = null): string
    {
        return $this->getValue(self::CAPTURE_ORDER_STATUS, $storeId);
    }

    /**
     * Returns invalid order status.
     *
     * @param int|null $storeId
     * @return string
     */
    public function getInvalidOrderStatus(?int $storeId = null): string
    {
        return $this->getValue(self::INVALID_ORDER_STATUS, $storeId);
    }
}
