<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Gateway\Http;

use Magento\Payment\Gateway\Http\TransferBuilder;
use Magento\Payment\Gateway\Http\TransferFactoryInterface;
use Magento\Payment\Gateway\Http\TransferInterface;

/**
 * Class TransferFactory
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Gateway\Http
 */
class TransferFactory implements TransferFactoryInterface
{
    /** @var TransferBuilder $transferBuilder */
    private $transferBuilder;

    /**
     * TransferFactory constructor.
     *
     * @param TransferBuilder $transferBuilder
     */
    public function __construct(
        TransferBuilder $transferBuilder
    ) {
        $this->transferBuilder = $transferBuilder;
    }

    /**
     * Builds gateway transfer object
     *
     * @param array $request
     * @return TransferInterface
     */
    public function create(array $request): TransferInterface
    {
        return $this->transferBuilder->setBody($request)->build();
    }
}
