<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Gateway\Request;

use Magento\Payment\Gateway\Request\BuilderInterface;

/**
 * Class Request
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Gateway\Request
 */
class Request implements BuilderInterface
{
    /**
     * Builds the request array.
     *
     * @param array $buildSubject
     * @return array
     */
    public function build(array $buildSubject): array
    {
        return [
            'payment' => $buildSubject['payment']->getPayment()
        ];
    }
}
