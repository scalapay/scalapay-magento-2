<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Gateway\Client;

use Exception;
use Magento\Payment\Gateway\Http\ClientException;
use Magento\Payment\Gateway\Http\ClientInterface;
use Magento\Payment\Gateway\Http\TransferInterface;
use Scalapay\Scalapay\Logger\Logger;
use Scalapay\Scalapay\Model\ApiClient;

/**
 * Class Capture
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Gateway\Client
 */
class Capture implements ClientInterface
{
    /** @var string CAPTURE_STATUS_OK */
    const CAPTURE_STATUS_OK = 'APPROVED';

    /** @var ApiClient $apiClient */
    private $apiClient;

    /** @var Logger $logger */
    private $logger;

    /**
     * Capture constructor.
     *
     * @param ApiClient $apiClient
     * @param Logger $logger
     */
    public function __construct(
        ApiClient $apiClient,
        Logger $logger
    ) {
        $this->apiClient = $apiClient;
        $this->logger = $logger;
    }

    /**
     * Capture the payment.
     *
     * @param TransferInterface $transferObject
     * @return array
     * @throws ClientException
     */
    public function placeRequest(TransferInterface $transferObject): array
    {
        try {
            // log
            $this->logger->info(__CLASS__ . ': Start');

            // get order
            $order = $transferObject->getBody()['payment']->getOrder();
            if (!$order || !$order->getId()) {
                throw new Exception('Invalid order.');
            }

            // get payment method
            $paymentMethod = $order->getPayment()->getMethod();
            if (!$paymentMethod) {
                throw new Exception('Invalid payment method.');
            }

            // get order token
            $orderToken = $order->getScalapayOrderToken();
            if (!$orderToken) {
                throw new Exception('Invalid order token.');
            }

            // get merchant reference
            $merchantReference = $order->getIncrementId();
            if (!$merchantReference) {
                throw new Exception('Invalid merchant reference.');
            }

            // log
            $this->logger->info(__CLASS__ . ': Order Id: ' . $order->getId());

            // set order token on order transaction
            $transferObject->getBody()['payment']->setTransactionId($orderToken);

            // get api client
            $orderStoreId = (int) $order->getStoreId() ?? null;
            $apiClient = $this->apiClient->execute($paymentMethod, $orderStoreId);

            // capture on Scalapay side
            $capture = $apiClient->capture($orderToken, $merchantReference);
            if (!isset($capture['body']['status'])
                || strtolower($capture['body']['status']) !== strtolower(self::CAPTURE_STATUS_OK)
            ) {
                $message = $capture['body']['message'] ?? 'Impossible capture the order ' . $order->getId();
                throw new Exception($message);
            }

            // log
            $this->logger->info(__CLASS__ . ': End');

            // return response
            return [];
        } catch (Exception $e) {
            // log exception
            $this->logger->critical(__CLASS__ . ': ' . $e->getMessage());

            // manually void the payment
            $this->invalidateCapture($transferObject);

            // throw exception again
            throw new ClientException(__('Payment capture was not authorized from Scalapay.'));
        }
    }

    /**
     * Invalidates the payment in case of exception during the capture phase.
     *
     * @param TransferInterface $transferObject
     * @return bool
     */
    private function invalidateCapture(TransferInterface $transferObject): bool
    {
        try {
            // log
            $this->logger->info(__CLASS__ . ': Void order start');

            // get order
            $order = $transferObject->getBody()['payment']->getOrder();
            if (!$order || !$order->getId()) {
                throw new Exception('Void - Invalid order.');
            }

            // get payment method
            $paymentMethod = $order->getPayment()->getMethod();
            if (!$paymentMethod) {
                throw new Exception('Void - Invalid payment method.');
            }

            // get order token
            $orderToken = $order->getScalapayOrderToken();
            if (!$orderToken) {
                throw new Exception('Void - Invalid order token.');
            }

            // get merchant reference
            $merchantReference = $order->getIncrementId();
            if (!$merchantReference) {
                throw new Exception('Void - Invalid merchant reference.');
            }

            // log
            $this->logger->info(__CLASS__ . ': Void - Order Id: ' . $order->getId());

            // get api client
            $orderStoreId = (int) $order->getStoreId() ?? null;
            $apiClient = $this->apiClient->execute($paymentMethod, $orderStoreId);

            // void on Scalapay side
            $invalidate = $apiClient->void($orderToken, $merchantReference);
            if (!isset($invalidate['body']['token']) || $invalidate['body']['token'] !== $orderToken) {
                throw new Exception('Impossible void the order ' . $order->getId());
            }

            // log
            $this->logger->info(__CLASS__ . ': Void order end');

            // return
            return true;
        } catch (Exception $e) {
            // log exception
            $this->logger->critical(__CLASS__ . ': ' . $e->getMessage());

            // return
            return false;
        }
    }
}
