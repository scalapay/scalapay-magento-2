<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Gateway\Client;

use Exception;
use Magento\Payment\Gateway\Http\ClientException;
use Magento\Payment\Gateway\Http\ClientInterface;
use Magento\Payment\Gateway\Http\TransferInterface;
use Magento\Sales\Model\Order\Creditmemo;
use Scalapay\Scalapay\Logger\Logger;
use Scalapay\Scalapay\Model\ApiClient;
use Scalapay\Sdk\Model\Refund\OrderRefund;

/**
 * Class Refund
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Gateway\Client
 */
class Refund implements ClientInterface
{
    /** @var ApiClient $apiClient */
    private $apiClient;

    /** @var Logger $logger */
    private $logger;

    /**
     * Refund constructor.
     *
     * @param ApiClient $apiClient
     * @param Logger $logger
     */
    public function __construct(
        ApiClient $apiClient,
        Logger $logger
    ) {
        $this->apiClient = $apiClient;
        $this->logger = $logger;
    }

    /**
     * Refund the payment.
     *
     * @param TransferInterface $transferObject
     * @return array
     * @throws ClientException
     */
    public function placeRequest(TransferInterface $transferObject): array
    {
        try {
            // log
            $this->logger->info(__CLASS__ . ': Start');

            // get order
            $order = $transferObject->getBody()['payment']->getOrder();
            if (!$order || !$order->getId()) {
                throw new Exception('Invalid order.');
            }

            // get payment method
            $paymentMethod = $order->getPayment()->getMethod();
            if (!$paymentMethod) {
                throw new Exception('Invalid payment method.');
            }

            // get order token
            $orderToken = $order->getScalapayOrderToken();
            if (!$orderToken) {
                throw new Exception('Invalid order token.');
            }

            // get credit memo
            $creditmemo = $transferObject->getBody()['payment']->getCreditmemo();
            if (!$creditmemo->getOrderId()) {
                throw new Exception('Invalid order credit memo.');
            }

            // get order refund object
            $orderRefundObject = $this->getOrderRefundObject($creditmemo);

            // log
            $this->logger->info(__CLASS__ . ': Order Id: ' . $order->getId());

            // get api client
            $orderStoreId = (int) $order->getStoreId() ?? null;
            $apiClient = $this->apiClient->execute($paymentMethod, $orderStoreId);

            // refund on Scalapay side
            $refund = $apiClient->refund($orderToken, $orderRefundObject);
            $refundOrderToken = $refund->getToken();
            if (!$refundOrderToken || $refundOrderToken !== $orderToken) {
                throw new Exception('Impossible refund the order ' . $order->getId());
            }

            // log
            $this->logger->info(__CLASS__ . ': End');

            // return response
            return [];
        } catch (Exception $e) {
            $this->logger->critical(__CLASS__ . ': ' . $e->getMessage());
            throw new ClientException(__('Refund was not authorized from Scalapay.'));
        }
    }

    /**
     * Returns the order refund object.
     *
     * @param Creditmemo $creditmemo
     * @return OrderRefund
     * @throws Exception
     */
    private function getOrderRefundObject(Creditmemo $creditmemo): OrderRefund
    {
        // instance order refund object
        $orderRefund = new OrderRefund();

        // set order refund properties
        $orderRefund->setRefundAmount((float) $creditmemo->getGrandTotal());
        $orderRefund->setMerchantReference($creditmemo->getOrderId());
        $orderRefund->setMerchantRefundReference('R' . $creditmemo->getOrder()->getIncrementId());

        // return order refund object
        return $orderRefund;
    }
}
