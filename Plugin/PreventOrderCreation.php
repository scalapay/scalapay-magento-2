<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Plugin;

use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Api\Data\PaymentInterface;
use Magento\Quote\Model\QuoteManagement;
use Scalapay\Scalapay\Helper\Payment as PaymentHelper;
use Scalapay\Scalapay\Logger\Logger;

/**
 * Class PreventOrderCreation
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Plugin
 */
class PreventOrderCreation
{
    /** @var CheckoutSession $checkoutSession */
    private $checkoutSession;

    /** @var CartRepositoryInterface $cartRepositoryInterface */
    private $cartRepositoryInterface;

    /** @var PaymentHelper $paymentHelper */
    private $paymentHelper;

    /** @var Logger $logger */
    private $logger;

    /**
     * PreventOrderCreation constructor.
     *
     * @param CheckoutSession $checkoutSession
     * @param CartRepositoryInterface $cartRepositoryInterface
     * @param PaymentHelper $paymentHelper
     * @param Logger $logger
     */
    public function __construct(
        CheckoutSession $checkoutSession,
        CartRepositoryInterface $cartRepositoryInterface,
        PaymentHelper $paymentHelper,
        Logger $logger
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->cartRepositoryInterface = $cartRepositoryInterface;
        $this->paymentHelper = $paymentHelper;
        $this->logger = $logger;
    }

    /**
     * Prevents the standard order creation at the pressure of the original 'Place Order' button in the checkout.
     *
     * @param QuoteManagement $subject
     * @param callable $proceed
     * @param $cartId
     * @param PaymentInterface|null $paymentMethod
     * @return int
     * @throws NoSuchEntityException
     */
    public function aroundPlaceOrder(
        QuoteManagement $subject,
        callable $proceed,
        $cartId,
        PaymentInterface $paymentMethod = null
    ): int {
        // get the quote and payment method
        $quote = $this->cartRepositoryInterface->get($cartId);
        $quotePaymentMethod = $quote->getPayment()->getMethod();

        // skip plugin if it is not a Scalapay payment method
        // or if the order is placing after the customer has been redirect on Confirm controller
        $isScalapayPayment = $this->paymentHelper->isScalapayPayment($quotePaymentMethod);
        $isScalapayOrderCreation = $this->checkoutSession->getIsScalapayOrderCreation();
        if (!$isScalapayPayment || $isScalapayOrderCreation) {
            // log
            if ($isScalapayPayment) {
                $this->logger->info(__CLASS__ . ': Create order - Quote id: ' . $quote->getId());
            }

            // proceed with order creation
            return (int) $proceed($cartId, $paymentMethod);
        }

        // log
        $this->logger->info(__CLASS__ . ': Skip order creation - Quote id: ' . $quote->getId());

        // reserve order id
        $quote->reserveOrderId();
        $this->cartRepositoryInterface->save($quote);
        return 0;
    }
}
