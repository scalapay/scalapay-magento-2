<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Plugin;

use Closure;
use Magento\Config\Model\Config;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Message\ManagerInterface as MessageManagerInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Scalapay\Scalapay\Gateway\Settings\Scalapay\Settings as ScalapaySettings;
use Scalapay\Scalapay\Helper\Config as ConfigHelper;
use Scalapay\Scalapay\Helper\Payment as PaymentHelper;
use Scalapay\Scalapay\Logger\Logger;
use Scalapay\Scalapay\Model\ApiConfiguration;

/**
 * Class CheckAdminConfig
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Plugin
 */
class CheckAdminConfig
{
    /** @var string PAYMENT_SECTION_ID */
    const PAYMENT_SECTION_ID = 'payment';

    /** @var ScopeConfigInterface $scopeConfigInterface */
    private $scopeConfigInterface;

    /** @var MessageManagerInterface $messageManagerInterface */
    private $messageManagerInterface;

    /** @var StoreManagerInterface $storeManagerInterface */
    private $storeManagerInterface;

    /** @var ScalapaySettings $scalapaySettings */
    private $scalapaySettings;

    /** @var ConfigHelper $configHelper */
    private $configHelper;

    /** @var PaymentHelper $paymentHelper */
    private $paymentHelper;

    /** @var Logger $logger */
    private $logger;

    /** @var ApiConfiguration $apiConfiguration */
    private $apiConfiguration;

    /** @var string $section */
    private $section;

    /** @var string $scopeType */
    private $scopeType;

    /** @var int $scopeId */
    private $scopeId;

    /**
     * CheckAdminConfig constructor.
     *
     * @param ScopeConfigInterface $scopeConfigInterface
     * @param MessageManagerInterface $messageManagerInterface
     * @param StoreManagerInterface $storeManagerInterface
     * @param ScalapaySettings $scalapaySettings
     * @param ConfigHelper $configHelper
     * @param PaymentHelper $paymentHelper
     * @param Logger $logger
     * @param ApiConfiguration $apiConfiguration
     */
    public function __construct(
        ScopeConfigInterface $scopeConfigInterface,
        MessageManagerInterface $messageManagerInterface,
        StoreManagerInterface $storeManagerInterface,
        ScalapaySettings $scalapaySettings,
        ConfigHelper $configHelper,
        PaymentHelper $paymentHelper,
        Logger $logger,
        ApiConfiguration $apiConfiguration
    ) {
        $this->scopeConfigInterface = $scopeConfigInterface;
        $this->messageManagerInterface = $messageManagerInterface;
        $this->storeManagerInterface = $storeManagerInterface;
        $this->scalapaySettings = $scalapaySettings;
        $this->configHelper = $configHelper;
        $this->paymentHelper = $paymentHelper;
        $this->logger = $logger;
        $this->apiConfiguration = $apiConfiguration;
    }

    /**
     * Checks min and max amount Scalapay configurations.
     *
     * @param Config $subject
     * @param Closure $proceed
     * @return mixed
     * @throws InputException
     * @throws NoSuchEntityException
     */
    public function aroundSave(
        Config $subject,
        Closure $proceed
    ) {
        // skip if it is not payment section
        if ($subject->getSection() !== self::PAYMENT_SECTION_ID) {
            return $proceed();
        }

        // set section type, scope type and scope ID
        $this->section = $subject->getSection();
        $this->scopeType = $this->getScopeType($subject);
        $this->scopeId = $this->getScopeId($subject);

        // loop configuration groups
        $configs = $subject->getGroups();
        foreach ($configs as $paymentMethod => $config) {
            // skip if it is not a Scalapay payment
            if (!$this->paymentHelper->isScalapayPayment($paymentMethod)) {
                continue;
            }

            // get mode
            $mode = (int) $this->getFieldValue($config, 'mode') ? 'production' : 'test';

            // get config min and max amount
            $configMinAmount = (float) $this->getFieldValue($config['groups']['payment_configurations'], 'min_amount');
            $configMaxAmount = (float) $this->getFieldValue($config['groups']['payment_configurations'], 'max_amount');

            // check min amount
            if ($configMinAmount < 5) {
                throw new InputException(
                    __(
                        'Invalid minimum amount (' . $configMinAmount . ') value ' .
                        '(' . $mode . ' mode): ' . 'Minimum amount value cannot be lower than 5.'
                    )
                );
            }

            // compare config min amount with config max amount
            if ($configMinAmount > $configMaxAmount) {
                throw new InputException(
                    __(
                        'Invalid minimum amount (' . $configMinAmount . ') ' .
                        'and maximum amount (' . $configMaxAmount . ') values ' .
                        '(' . $mode . ' mode): ' . 'Minimum amount value cannot be greater than maximum amount value.'
                    )
                );
            }

            // TODO: actually do not perform other checks and return
            return $proceed();

            // get Scalapay API configurations
            $settings = $this->getSettings($configs);
            $apiConfigs = $this->apiConfiguration->get($settings);

            // get and check api amount thresholds
            $amountThresholds = $this->getAmountThresholds($apiConfigs);
            if (!count($amountThresholds)) {
                // add warning message in the case of the API is invalid or the API service is down
                $this->messageManagerInterface->addWarningMessage(
                    __(
                        'Impossible connect to Scalapay API. Maybe the API key is invalid? ' .
                        '[Mode: ' . $mode . ']'
                    )
                );

                // proceed
                return $proceed();
            }

            // get min and max api amount thresholds
            $apiMinAmountThreshold = $amountThresholds[$mode]['minAmount'];
            $apiMaxAmountThreshold = $amountThresholds[$mode]['maxAmount'];

            // compare config min amount with api min amount
            if ($configMinAmount < $apiMinAmountThreshold) {
                throw new InputException(
                    __(
                        'Invalid minimum amount value (' . $configMinAmount . ') ' .
                        ' (' . $mode . ' mode): Insert a value greater than or equal to ' .
                        $apiMinAmountThreshold . '.'
                    )
                );
            }

            // compare config max amount with api max amount
            if ($configMaxAmount > $apiMaxAmountThreshold) {
                throw new InputException(
                    __(
                        'Invalid maximum amount value (' . $configMaxAmount . ') ' .
                        ' (' . $mode . ' mode): Insert a value less than or equal to ' .
                        $apiMaxAmountThreshold . '.'
                    )
                );
            }
        }

        // proceed
        return $proceed();
    }

    /**
     * Returns the Scalapay amount API min and max thresholds.
     *
     * @param array $apiConfigs
     * @return array
     */
    private function getAmountThresholds(array $apiConfigs): array
    {
        // init vars
        $amountThresholds = [];

        // loop scalapay api configurations
        foreach ($apiConfigs as $mode => $apiConfig) {
            foreach ($apiConfig as $productType => $config) {
                // get min and max api amounts
                $apiMinAmount = (float) $this->getApiValue($apiConfigs, $mode, $productType, 'minAmount');
                $apiMaxAmount = (float) $this->getApiValue($apiConfigs, $mode, $productType, 'maxAmount');

                // check api min and max amount
                if (!$apiMinAmount || !$apiMaxAmount) {
                    continue;
                }

                // set the lower min amount
                if (!isset($amountThresholds[$mode]['minAmount']) ||
                    $apiMinAmount < $amountThresholds[$mode]['minAmount']
                ) {
                    $amountThresholds[$mode]['minAmount'] = $apiMinAmount;
                }

                // set the higher max amount
                if (!isset($amountThresholds[$mode]['maxAmount']) ||
                    $apiMaxAmount > $amountThresholds[$mode]['maxAmount']
                ) {
                    $amountThresholds[$mode]['maxAmount'] = $apiMaxAmount;
                }
            }
        }

        // return amount thresholds
        return $amountThresholds;
    }

    /**
     * Returns the current scope type.
     *
     * @param Config $subject
     * @return string
     */
    private function getScopeType(Config $subject): string
    {
        // detect website scope
        if ($subject->getWebsite()) {
            return ScopeInterface::SCOPE_WEBSITE;
        }

        // detect store scope
        if ($subject->getStore()) {
            return ScopeInterface::SCOPE_STORE;
        }

        // detect default scope
        return 'default';
    }

    /**
     * Returns level up scope as inherited scope type by the given current scope type.
     * Hierarchy: Default > Website > Store
     *
     * @param string $currentScopeType
     * @return string
     * @throws InputException
     */
    private function getInheritedScopeType(string $currentScopeType): string
    {
        switch ($currentScopeType) {
            case ScopeInterface::SCOPE_WEBSITE:
                return 'default';

            case ScopeInterface::SCOPE_STORE:
                return ScopeInterface::SCOPE_WEBSITE;

            default:
                throw new InputException(__('Impossible retrieve the inherited scope type.'));
        }
    }

    /**
     * Returns the current scope id.
     *
     * @param Config $subject
     * @return int
     */
    private function getScopeId(Config $subject): int
    {
        // retrieve scope type
        if (!$this->scopeType) {
            $this->scopeType = $this->getScopeType($subject);
        }

        // return scope id
        // value 0 is default scope
        switch ($this->scopeType) {
            case ScopeInterface::SCOPE_WEBSITE:
                return (int) $subject->getWebsite();

            case ScopeInterface::SCOPE_STORE:
                return (int) $subject->getStore();

            default:
                return 0;
        }
    }

    /**
     * Returns level up scope id as inherited scope id by the given current scope data.
     * Hierarchy: Default > Website > Store
     *
     * @param string $currentScopeType
     * @param int $currentScopeId
     * @return int
     * @throws InputException
     * @throws NoSuchEntityException
     */
    private function getInheritedScopeId(
        string $currentScopeType,
        int $currentScopeId
    ): int {
        switch ($currentScopeType) {
            case ScopeInterface::SCOPE_WEBSITE:
                // retrieve default id
                return (int) $this->storeManagerInterface->getDefaultStoreView()->getId();

            case ScopeInterface::SCOPE_STORE:
                // retrieve website id
                return (int) $this->storeManagerInterface->getStore($currentScopeId)->getWebsiteId();

            default:
                throw new InputException(__('Impossible retrieve the inherited scope id.'));
        }
    }

    /**
     * Returns the settings to call the API endpoints based on the inserted data.
     *
     * @param $configs
     * @return array
     */
    private function getSettings($configs): array
    {
        // return settings array
        foreach ($configs as $paymentMethod => $config) {
            // skip if it is not a Scalapay payment
            if (!$this->paymentHelper->isScalapayPayment($paymentMethod)) {
                continue;
            }

            // return settings
            return [
                'test' => [
                    'endpoint' => $this->scalapaySettings->getTestUrl(),
                    'secretKey' => $this->getFieldValue($config, 'test_api_key') ??
                        $this->scalapaySettings->getTestApiKey()
                ],
                'production' => [
                    'endpoint' => $this->scalapaySettings->getProductionUrl(),
                    'secretKey' => $this->getFieldValue($config, 'production_api_key') ??
                        $this->scalapaySettings->getProductionApiKey()
                ]
            ];
        }

        // return
        return [];
    }

    /**
     * Returns an api value.
     *
     * @param array $apiConfiguration
     * @param string $mode
     * @param string $productType
     * @param string $value
     * @return mixed
     */
    private function getApiValue(
        array $apiConfiguration,
        string $mode,
        string $productType,
        string $value
    ) {
        // check configuration
        if (!isset($apiConfiguration[$mode][$productType][$value])) {
            // log data
            $this->logger->critical(
                __CLASS__ . ': Invalid configuration - Mode: ' . $mode .
                ' - Product Type: ' . $productType . ' - Value: ' . $value
            );

            return false;
        }

        // return api value
        return $apiConfiguration[$mode][$productType][$value];
    }

    /**
     * Returns a page configuration field value.
     * Inherited configuration by the store or website have 'inherit' key instead of 'value' key.
     * The 'inherit' key is only a flag, it doesn't contain the value, so it is necessary recover it via db.
     *
     * @param array $config
     * @param string $key
     * @return mixed
     * @throws InputException
     * @throws NoSuchEntityException
     */
    private function getFieldValue(
        array $config,
        string $key
    ) {
        if (isset($config['fields'][$key]['inherit'])) {
            return $this->scopeConfigInterface->getValue(
                $this->section . '/' . ScalapaySettings::CODE . '/' . $key,
                $this->getInheritedScopeType($this->scopeType),
                $this->getInheritedScopeId($this->scopeType, $this->scopeId)
            );
        }

        return $config['fields'][$key]['value'] ?? null;
    }
}
