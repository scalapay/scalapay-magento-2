<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Plugin;

use Exception;
use Magento\Payment\Api\Data\PaymentMethodInterface;
use Magento\Payment\Model\MethodInterface;
use Magento\Payment\Model\MethodList;
use Magento\Quote\Api\Data\CartInterface;
use Scalapay\Scalapay\Helper\Payment as PaymentHelper;
use Scalapay\Scalapay\Model\FilterProcessor;

/**
 * Class DisablePaymentMethods
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Plugin
 */
class DisablePaymentMethods
{
    /** @var FilterProcessor $filterProcessor */
    private $filterProcessor;

    /** @var PaymentHelper $paymentHelper */
    private $paymentHelper;

    /**
     * DisablePaymentMethod constructor.
     *
     * @param FilterProcessor $filterProcessor
     * @param PaymentHelper $paymentHelper
     */
    public function __construct(
        FilterProcessor $filterProcessor,
        PaymentHelper $paymentHelper
    ) {
        $this->filterProcessor = $filterProcessor;
        $this->paymentHelper = $paymentHelper;
    }

    /**
     * Removes the Scalapay payment method if it doesn't pass the filter checks.
     *
     * @param MethodList $subject
     * @param array $availableMethods
     * @param CartInterface|null $quote
     * @return MethodInterface[]
     * @throws Exception
     */
    public function afterGetAvailableMethods(
        MethodList $subject,
        array $availableMethods,
        CartInterface $quote = null
    ): array {
        /** @var PaymentMethodInterface $paymentMethod */
        foreach ($availableMethods as $key => $paymentMethod) {
            // get payment code
            $paymentCode = $paymentMethod->getCode();

            // skip if it is not a scalapay payment
            if (!$this->paymentHelper->isScalapayPayment($paymentCode)) {
                continue;
            }

            // apply filters
            if (!$this->filterProcessor->execute($quote)) {
                unset($availableMethods[$key]);
            }
        }

        // shift array keys and return available methods
        $availableMethods = array_values($availableMethods);
        return $availableMethods;
    }
}
